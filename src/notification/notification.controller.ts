import { Controller, forwardRef, Get, Inject, Param, Post, Res } from '@nestjs/common';
import { ResponseObj } from 'src/utils/interfaces/ResponseObj';
import { NotificationService } from './notification.service';
import { Response } from 'express';
import { Body } from '@nestjs/common/decorators/http/route-params.decorator';
export enum NotificationType {
    FOLLOW = "follow",
    LIKE = "like",
    COMMENT = "comment",
    APP = "app",
    MENTION = "mention",
    ANSWER = "answer",
    INTERESTASK = "interestAsk",
    DIRECTQUESTION = "directQuestion"

}

export enum FollowStatusNotification {
    PENDING = 'pending',
    ACCEPTED = "accepted",
    DECLINED = "declined"
}

@Controller('notification')
export class NotificationController {

    constructor(
        @Inject(forwardRef(() => NotificationService))
        private notificationService: NotificationService
    ) { }


    @Get('getUserNotifications/:id')
    async getUserNotifications(@Param('id') id: string, @Res() res: Response): Promise<any> {
        res.status(200).json(await this.notificationService.getUserNotifications(id));
    }

    @Post('test')
    async test(@Body() body, @Res() res: Response): Promise<any> {
        const responseObj: ResponseObj = await this.notificationService.createCommentNotification(body.userId, body.questionId, body.comment);
        res.status(responseObj.code).json(responseObj.data);
    }



}
