import { Controller, Post, Res, Body, HttpService } from "@nestjs/common";
import { Response } from 'express';

const ejabberdApiUrl = 'http://db.stoppoint.com:5280/api';
const ejjaberdAdminUser = 'admin@videoapp';
const ejjaberdAdminPassword = 'admin';

//This controller contains all ejabberd API calls using HttpService axios
@Controller('chat')
export class EjabberdController {
    constructor(private httpService: HttpService) { }

    async xmppRegister(body: any): Promise<any> {
        try {
            const data = await this.httpService.post(ejabberdApiUrl + '/register', {
                user: body.user,
                host: body.host,
                password: body.password
            }, {
                auth: {
                    username: ejjaberdAdminUser,
                    password: ejjaberdAdminPassword
                }
            }).toPromise();

            return {
                statusCode: data.status,
                statusText: data.statusText,
                data: data.data
            }
        } catch (err) {
            return {
                statusCode: err.response.status,
                statusText: err.response.statusText,
                data: err.response.data.message
            }
        }
    }

    @Post('register')
    private async xmppRegisterService(@Body() body, @Res() res: Response): Promise<any> {
        try {
            const data = await this.httpService.post(ejabberdApiUrl + '/register', {
                user: body.user,
                host: body.host,
                password: body.password
            }, {
                auth: {
                    username: ejjaberdAdminUser,
                    password: ejjaberdAdminPassword
                }
            }).toPromise();
            res.status(data.status).json({
                statusCode: data.status,
                statusText: data.statusText,
                data: data.data
            });
        } catch (err) {
            res.status(err.response.status).json({
                statusCode: err.response.status,
                statusText: err.response.statusText,
                data: err.response.data.message
            });
        }
    }

    @Post('sendMessage')
    private async xmppSendMessageService(@Body() body, @Res() res: Response): Promise<any> {
        try {
            const data = await this.httpService.post(ejabberdApiUrl + '/send_message', {
                type: body.type,
                from: body.from,
                to: body.to,
                subject: body.subject,
                body: body.body
            }, {
                auth: {
                    username: ejjaberdAdminUser,
                    password: ejjaberdAdminPassword
                }
            }).toPromise();
            res.status(data.status).json({
                statusCode: data.status,
                statusText: data.statusText,
                data: data.data
            });
        } catch (err) {
            res.status(err.response.status).json({
                statusCode: err.response.status,
                statusText: err.response.statusText,
                data: err.response.data.message
            });
        }
    }

    @Post('getLast')
    private async xmppGetLast(@Body() body, @Res() res: Response): Promise<any> {
        try {
            const data = await this.httpService.post(ejabberdApiUrl + '/get_last', {
                user: body.user,
                host: body.host
            }, {
                auth: {
                    username: ejjaberdAdminUser,
                    password: ejjaberdAdminPassword
                }
            }).toPromise();
            res.status(data.status).json({
                statusCode: data.status,
                statusText: data.statusText,
                data: data.data
            });
        } catch (err) {
            res.status(err.response.status).json({
                statusCode: err.response.status,
                statusText: err.response.statusText,
                data: err.response.data.message
            });
        }
    }
}