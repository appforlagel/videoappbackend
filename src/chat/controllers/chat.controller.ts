import { Controller, Get, Post, Res, Param, Body, Query } from "@nestjs/common";
import { Response } from 'express';
import { ChatModel } from '../models/chat';
import { isUUID } from 'validator';
import { xml2json } from 'xml-js';
import { UserService } from 'src/auth/services/user.service';

@Controller('chat')
export class ChatController {
    constructor(private chatModel: ChatModel, private userService: UserService) { }

    @Get('getLastByKind')
    private async xmppGetLastByKindService(@Query('username') username, @Query('maxConversations') maxConversations, @Query('maxMessages') maxMessages, @Query('kind') kind, @Res() res: Response): Promise<any> {
        try {
            const data = await this.chatModel.dbGetLastByKind(username, maxConversations, maxMessages, kind);
            const toUsers: string[] = [...new Set<string>(data.map((x: any) => x.bare_peer))];
            const userProfile = await this.userService.getSimpleProfile(username);
            const payload = [];

            for (let i = 0; i < toUsers.length; i++) {
                if (isUUID(toUsers[i].split("@")[0])) { // UUID@SERVER_DOMAIN
                    const user = await this.userService.getSimpleProfile(toUsers[i].split("@")[0]);
                    const chatWith = { id: user.data.id, firstname: user.data.firstname, lastname: user.data.lastname, avatar: user.data.avatar, email: user.data.email };
                    payload.push({
                        chatWith: chatWith,
                        messages: data.filter((x: any) => x.bare_peer == toUsers[i])
                            .map((x: any) => {
                                const xmlParse = JSON.parse(xml2json(x.xml));
                                return {
                                    //username: x.username,
                                    //barePeer: x.bare_peer,
                                    //xml: x.xml,
                                    txt: x.txt,
                                    createdAt: x.created_at,
                                    from: xmlParse.elements.find((x: any) => x.name == "message").attributes.from.split('@')[0] == userProfile.data.id ?
                                        { id: userProfile.data.id, firstname: userProfile.data.firstname, lastname: userProfile.data.lastname, avatar: userProfile.data.avatar, email: userProfile.data.email } :
                                        chatWith
                                }
                            }).reverse()
                    })
                }
            }
            res.status(200).json({
                chatSessions: payload
            })
        } catch (err) {
            console.log(err);
            res.status(500).json({
                statusCode: 500,
                data: err
            });
        }
    }

    @Get('getByKindFromTo')
    private async xmppGetByKindFromToService(@Query('username') username, @Query('contact') contact, @Query('limit') limit, @Query('offset') offset, @Query('kind') kind, @Res() res: Response) {
        try {
            const userProfile = await this.userService.getSimpleProfile(username);
            const contactProfile = await this.userService.getSimpleProfile(contact.split('@')[0]);
            const data = await this.chatModel.dbGetByKindFromTo(username, contact, limit, offset, kind);

            res.status(200).json({
                chatWith: { id: contactProfile.data.id, firstname: contactProfile.data.firstname, lastname: contactProfile.data.lastname, avatar: contactProfile.data.avatar, email: contactProfile.data.email },
                messages: data.map((x: any) => {
                    const xmlParse = JSON.parse(xml2json(x.xml));
                    return {
                        txt: x.txt,
                        createdAt: x.created_at,
                        from: xmlParse.elements.find((x: any) => x.name == "message").attributes.from.split('@')[0] == userProfile.data.id ?
                            { id: userProfile.data.id, firstname: userProfile.data.firstname, lastname: userProfile.data.lastname, avatar: userProfile.data.avatar, email: userProfile.data.email } :
                            { id: contactProfile.data.id, firstname: contactProfile.data.firstname, lastname: contactProfile.data.lastname, avatar: contactProfile.data.avatar, email: contactProfile.data.email }
                    }

                }).reverse()
            });
        } catch (err) {
            console.log(err);
            res.status(500).json({
                statusCode: 500,
                data: err
            });
        }
    }

    @Get('deleteByKindFromTo')
    private async xmppDeleteByKindFromToService( @Query('contact') contact,  @Query('kind') kind, @Res() res: Response) {
        try {
            const data = await this.chatModel.dbDeleteByKindFromTo( contact, kind);
            if(data){
                res.status(200).json({
                    status: true,
                    message:"Chat Successfully Deleted "
                });
            }else{
                res.status(500).json({
                    status: false,
                    message:"Error in Deleting Chat "
                });
            }
           
        } catch (err) {
            console.log(err);
            res.status(500).json({
                statusCode: 500,
                data: err
            });
        }
    }
}
