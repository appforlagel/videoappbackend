import { Module, HttpModule } from '@nestjs/common';
import { NestPgpromiseModule } from './pgpromise/nest-pgpromise.module';
import { ChatController } from './controllers/chat.controller';
import { EjabberdController } from './controllers/ejabberd.controller';
import { ChatModel } from './models/chat';
import { AuthModule } from 'src/auth/auth.module';
import { UserService } from 'src/auth/services/user.service';
import { AnswerService } from 'src/answer/answer.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Answer } from 'src/models/postgresEntities/AnswerEntity';
import { Question } from 'src/models/postgresEntities/QuestionEntity';
import { User } from 'src/models/postgresEntities/UserEntity';
import { VoteToComment } from 'src/models/postgresEntities/VoteToCommentEntity';
import { Comment } from 'src/models/postgresEntities/CommentEntity';

@Module({
    controllers: [ChatController, EjabberdController],
    providers: [ChatModel, UserService, AnswerService],
    imports: [
        HttpModule,
        AuthModule,
        TypeOrmModule.forFeature([User, Comment, Answer, Question, VoteToComment]),
        NestPgpromiseModule.register({
            /*connection: {
                host: 'localhost',
                port: 5432,
                database: 'ejabberdDB',
                user: 'postgres',
                password: 'postgres',
            },*/
            connection: {
                host: 'db.stoppoint.com',
                port: 5432,
                database: 'ejabberdDB',
                user: 'videoApp',
                password: 'videoApp2020',
            },
        })
    ],
})
export class ChatModule { 

    
}
