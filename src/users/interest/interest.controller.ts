import { Controller, Post, Body, Res, Put, Param, Delete, Get } from '@nestjs/common';
import { Response } from 'express';
import { InterestService } from '../interest/interest.service';
import { ResponseObj } from 'src/utils/interfaces/ResponseObj';
@Controller('interest')
export class InterestController {

    constructor(private interestService: InterestService) { }

    @Post()
    async create(@Body() interest, @Res() res: Response): Promise<any> {
        const responseObj: ResponseObj = await this.interestService.create(interest);
        res.status(responseObj.code).json(responseObj.data);
    }

    @Get()
    async findAll(@Res() res: Response): Promise<any> {
        const responseObj: ResponseObj = await this.interestService.findAll();
        res.status(responseObj.code).json(responseObj.data);
    }

    @Post('addInterests/:id')
    async addInterestToUser(@Body() interstList, @Param() params, @Res() res: Response): Promise<any> {
        const responseObj: ResponseObj = await this.interestService.addInterestToUser(interstList, params.id);
        res.status(responseObj.code).json(responseObj.data);
    }

    @Post('follow')
    async followInterest(@Body() body, @Res() res: Response): Promise<any> {
        const responseObj: ResponseObj = await this.interestService.followInterest(body);
        res.status(responseObj.code).json(responseObj.data);
    }

    @Post('unfollow')
    async unfollowInterest(@Body() body, @Res() res: Response): Promise<any> {
        const responseObj: ResponseObj = await this.interestService.unfollowInterest(body);
        res.status(responseObj.code).json(responseObj.data);
    }

    @Get('InterestsWithVideos')
    async getInterestswithVideos(@Res() res: Response): Promise<any> {
        const responseObj: ResponseObj = await this.interestService.getInterestswithVideos();
        res.status(responseObj.code).json(responseObj.data);
    }


    // @Put(':id')
    // async update(@Body() user, @Res() res: Response, @Param() params): Promise<any> {
    //     console.log('epic');
    //     res.status(200).json({ "lolito": "looool" });
    // }

    // @Delete(':id')
    // async remove(@Param('id') id: string, @Res() res: Response): Promise<any> {
    //     const responseObj: ResponseObj = await this.interestService.delete(id);
    //     res.status(responseObj.code).json(responseObj.data);
    // }
}
