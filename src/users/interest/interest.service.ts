import { Injectable } from '@nestjs/common';
import { neode } from 'src/main';
import { ResponseObj } from 'src/utils/interfaces/ResponseObj';


@Injectable()
export class InterestService {

    async findAll(): Promise<any> {
        let responseObj: ResponseObj;
        return await neode.all('Interest').then(
            async (res) => {
                responseObj = { code: 200, data: await res.toJson() };
                return responseObj;
            }
        ).catch(
            (err) => {
                console.log(err);
                responseObj = { code: 500, data: err };
                return responseObj;
            }
        )
    }

    async create(interest): Promise<any> {
        let responseObj: ResponseObj;
        const interestDb = await neode.first('Interest', { label: interest.label }, 1);

        if (!interestDb) {
            return await neode.merge('Interest', interest).then(
                async (createdRes) => {
                    responseObj = { code: 201, data: await createdRes.toJson() };
                    return responseObj;
                },
            ).catch(
                (err: any) => {
                    console.log('errordd ');
                    console.log(err);
                    responseObj = { code: 400, data: err.toString() };
                    return responseObj;
                },
            );
        } else {
            responseObj = { code: 422, data: 'The interest is already registered' };
            return responseObj;
        }


    }

    async addInterestToUser(interestList: string[], userId: string) {
        let responseObj: ResponseObj;
        const userDb = await neode.find('User', userId);

        if (userDb) {
            return await neode.writeCypher(`
                MATCH (u:User {id:$userId}) -[ui:INTERESTED_IN]-> (i: Interest) detach delete ui
            `, { userId: userId }).then(
                (res) => {
                    interestList.forEach(async (interestId) => {
                        const interestDb = await neode.find('Interest', interestId);
                        await userDb.relateTo(interestDb, 'interests');
                    });
                    responseObj = { code: 200, data: 'Interest added' };
                    return responseObj
                }).catch(
                    err => {
                        console.log(err);
                        responseObj = { code: 500, data: 'Database error' };
                        return responseObj
                    }
                );

        } else {
            responseObj = { code: 404, data: 'The user is not found in the database' };
            return responseObj
        }
    }

    async followInterest(body) {
        let responseObj: ResponseObj;
        try {

            const res = await neode.writeCypher(`
                    MATCH (u:User {id:$userId}) 
                    MATCH (i:Interest {id: $interestId})
                    MERGE (u)-[ir:INTERESTED_IN] -> (i)
                    return  properties(i) as interest
                `, { userId: body.userId, interestId: body.interestId });


            responseObj = { code: 200, data: res.records[0].get('interest') };
            return responseObj

        } catch (e) {
            console.log(e);
            responseObj = { code: 500, data: e };
            return responseObj
        }
    }



    async unfollowInterest(body) {
        let responseObj: ResponseObj;
        try {

            const res = await neode.writeCypher(`
                    MATCH (u:User {id:$userId}) -[ir:INTERESTED_IN]->(i:Interest {id: $interestId})
                    detach delete ir return properties(i) as interest
                `, { userId: body.userId, interestId: body.interestId });
            console.log(res);
            responseObj = { code: 200, data: res.records[0].get('interest') };
            return responseObj

        } catch (e) {
            console.log(e);
            responseObj = { code: 500, data: e };
            return responseObj
        }
    }

    async getInterestswithVideos() {
        let responseObj: ResponseObj;
        try {
            const result = [];
            const res = await neode.cypher(`
                MATCH (i:Interest )
                OPTIONAL MATCH (i)<-[:QUESTIONS_RECEIVED]- (q:Question)
                OPTIONAL MATCH (q)-[:ANSWER]->(a:Answer)
                WHERE q.lastAnswer = a.createdAt
                WITH 
                q{.*, answer: properties(a)},i
                return properties(i) as interest,collect (q) as questions order by interest.label          
                `, {});

            res.records.forEach((i) => {
                const aux = {
                    interest: {},
                    videos: []
                };
                aux.interest = i.get('interest');
                if (i.get('questions')[0] != null && i.get('questions')[0].answer != null) {
                    aux.videos = i.get('questions');
                }
                result.push(aux);
            })
            responseObj = { code: 200, data: result };
            return responseObj

        } catch (e) {
            console.log(e);
            responseObj = { code: 500, data: e };
            return responseObj
        }

    }


}
