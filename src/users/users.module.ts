import { Module } from '@nestjs/common';
import { InterestController } from './interest/interest.controller';
import { InterestService } from './interest/interest.service';

@Module({
  controllers: [InterestController],
  providers: [InterestService]
})
export class UsersModule { }
