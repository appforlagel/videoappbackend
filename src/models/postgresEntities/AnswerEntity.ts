import { Comment } from './CommentEntity';
import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';


@Entity()
export class Answer {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: 'uuid', unique: true })
    answerId: string;

    @OneToMany(type => Comment, commets => commets.parentAnswer)
    comments: Comment[];

}