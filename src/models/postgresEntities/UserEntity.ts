import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { Comment } from './CommentEntity';
import { VoteToComment } from './VoteToCommentEntity';

@Entity()
export class User {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: 'uuid', unique: true })
    userId: string;

    @OneToMany(type => Comment, commets => commets.user)
    comments: Comment[];

    @OneToMany(type => VoteToComment, voteToComment => voteToComment.user)
    public votes!: VoteToComment[];
}
