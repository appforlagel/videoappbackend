import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { Comment } from './CommentEntity';



@Entity()
export class Question {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: 'uuid', unique: true })
    questionId: string;

    @OneToMany(type => Comment, commets => commets.question)
    comments: Comment[];

}