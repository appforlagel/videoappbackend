import { Entity, Column, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { User } from "./UserEntity";
import { Comment } from './CommentEntity';
export enum VoteType {
    UPVOTE = "upvote",
    DOWNVOTE = "downvote",
}
@Entity()
export class VoteToComment {
    @PrimaryGeneratedColumn()
    public voteToCommentId!: number;

    @Column()
    public commentId!: number;

    @Column()
    public userId!: number;

    @Column('timestamptz')
    date: Date;

    @Column({
        type: "enum",
        enum: VoteType,
    })
    voteType: VoteType

    @ManyToOne(type => User, user => user.votes)
    public user!: User;

    @ManyToOne(type => Comment, comment => comment.votes)
    public comment!: Comment;
}