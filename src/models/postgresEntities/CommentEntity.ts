import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, ManyToMany, JoinColumn, JoinTable, OneToMany } from 'typeorm';
import { User } from './UserEntity';
import { Question } from './QuestionEntity';
import { VoteToComment } from './VoteToCommentEntity';
import { Answer } from './AnswerEntity';

@Entity()
export class Comment {
    @PrimaryGeneratedColumn()
    id: number;

    @Column('text')
    body: string;

    @Column('timestamptz')
    date: Date;

    @ManyToOne(type => User, user => user.comments)
    user: User;

    @ManyToOne(type => Question, question => question.comments)
    question: Question;

    @ManyToOne(type => Answer, answer => answer.comments)
    parentAnswer: Answer;

    @OneToMany(type => Comment, answers => answers.parentComment)
    answers: Comment[];

    @ManyToOne(type => Comment, comment => comment.answers,)
    parentComment: Comment;

    @OneToMany(type => VoteToComment, voteToComment => voteToComment.comment)
    public votes!: VoteToComment[];
}