import { Injectable } from '@nestjs/common';
import { neode } from 'src/main';
import { ResponseObj } from 'src/utils/interfaces/ResponseObj';

@Injectable()
export class ExploreService {

    async getInformation(body, userId) {
        let responseObj: ResponseObj;
        return neode.cypher(
            `MATCH (q:Question) WHERE  replace(toLower(q.text), " ", "") =~ ('.*' + $text +'.*')  
             OPTIONAL MATCH (q)-[:QUESTIONS_RECEIVED]->(i:Interest) with properties(i) as interest, q return q {.*, interest} as data
             UNION
             MATCH (i:Interest) WHERE replace (toLower(i.label), " ", "") =~ ('.*' + $text +'.*') return i {.*} as data
             UNION
             MATCH (ur:User {id:$userId})
             MATCH (u: User ) WHERE (replace (toLower(u.firstname), " ", "") =~ ('.*' + $text +'.*') OR replace (toLower(u.lastname), " ", "") =~ ('.*' + $text +'.*') 
             OR replace (toLower(u.firstname + u.lastname), " ", "") =~ ('.*' + $text +'.*'))  and not exists((u)-[:BLOCK]->(ur))
             return u {.*} as data
            `
            , { text: body.text.toLowerCase().split(" ").join(""), userId: userId })
            .then(
                res => {
                    const result = [];
                    res.records.forEach((x) => {
                        result.push(x.get('data'));
                    })
                    responseObj = {
                        code: 200,
                        data: result
                    }
                    return responseObj
                }
            )
    }
}
