import { Controller, Post, UseGuards } from '@nestjs/common';
import { Body, Res } from '@nestjs/common/decorators/http/route-params.decorator';
import { ResponseObj } from 'src/utils/interfaces/ResponseObj';
import { Response } from 'express';
import { ExploreService } from './explore.service';
import { AuthGuard } from '@nestjs/passport';
import { AuthUser } from 'src/auth/user.decorator';
@Controller('explore')
export class ExploreController {

    constructor(private _exploreService: ExploreService) { }

    @Post('getInformation')
    @UseGuards(AuthGuard('jwt'))
    async getInformation(@Body() body, @Res() res: Response, @AuthUser() user: any): Promise<any> {
        const responseObj: ResponseObj = await this._exploreService.getInformation(body, user.userId);
        res.status(responseObj.code).json(responseObj.data);
    }

}
