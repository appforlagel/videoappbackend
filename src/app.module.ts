import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { QuestionsModule } from './questions/questions.module';
import { FeedModule } from './feed/feed.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './models/postgresEntities/UserEntity';
import { Comment } from './models/postgresEntities/CommentEntity';
import { Question } from './models/postgresEntities/QuestionEntity';
import { VoteToComment } from './models/postgresEntities/VoteToCommentEntity';
import { NotificationModule } from './notification/notification.module';
import { FcmModule } from 'nestjs-fcm';
import * as path from 'path';
import { ChatModule } from './chat/chat.module';
import { Answer } from './models/postgresEntities/AnswerEntity';
import { AnswerModule } from './answer/answer.module';
import { ExploreModule } from './explore/explore.module';
import { CloudinaryModule } from './cloudinary/cloudinary.module';
import { Admin } from './models/postgresEntities/AdminEntity';


@Module({
  imports: [
    NotificationModule,
    TypeOrmModule.forRoot({
      "type": "postgres",
      "host": "db.stoppoint.com",
      "port": 5432,
      "username": "videoApp",
      "password": "videoApp2020",
      "database": "VideoApp",
      "entities": [
        User,
        Comment,
        Question,
        VoteToComment,
        Answer,
        Admin
      ],
      "synchronize": true
    }),
    AuthModule,
    UsersModule,
    FeedModule,
    QuestionsModule,
    FcmModule.forRoot({
      firebaseSpecsPath: path.join(__dirname, '../firebase.spec.json'),
    }),
    ChatModule,
    AnswerModule,
    ExploreModule,
    CloudinaryModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
