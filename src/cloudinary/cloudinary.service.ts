import { Injectable, Inject } from '@nestjs/common';
import { Cloudinary } from './cloudinary';
@Injectable()
export class CloudinaryService {
    private v2: any
    constructor(
        @Inject(Cloudinary)
        private cloudinary
    ) {
        this.cloudinary.v2.config({
            cloud_name: 'stoppoint',
            api_key: '448182769163657',
            api_secret: 'fj1WSnsts5rka3q63JcoQKNW_zo'
        })
        this.v2 = cloudinary.v2
    }
    async upload(publicId: string) {
        return await this.v2.uploader.upload(publicId, { resource_type: "video" })
    }

    async delete(publicId: string) {
        return await this.v2.uploader.destroy(publicId, { resource_type: "video" });
    }
}