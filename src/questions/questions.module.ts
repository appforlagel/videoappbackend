import { Module } from '@nestjs/common';
import { QuestionController } from './controllers/question.controller';
import { QuestionService } from './services/question.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from 'src/models/postgresEntities/UserEntity';
import { Question } from 'src/models/postgresEntities/QuestionEntity';
import { Comment } from 'src/models/postgresEntities/CommentEntity';
import { VoteToComment } from 'src/models/postgresEntities/VoteToCommentEntity';
import { FeedService } from 'src/feed/feed.service';
import { AnswerService } from 'src/answer/answer.service';
import { Answer } from 'src/models/postgresEntities/AnswerEntity';
import { CloudinaryModule } from 'src/cloudinary/cloudinary.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([User, Comment, Answer, Question, VoteToComment]),
    CloudinaryModule
  ],
  controllers: [QuestionController],
  providers: [QuestionService, FeedService, AnswerService]
})
export class QuestionsModule { }
