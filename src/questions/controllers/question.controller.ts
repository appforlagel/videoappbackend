import { Controller, Post, Body, Res, Get, Param, Delete, Put } from '@nestjs/common';
import { QuestionService } from '../services/question.service';
import { ResponseObj } from 'src/utils/interfaces/ResponseObj';
import { Response } from 'express';
export enum QuestionType {
    USER_QUESTION,
    GENERAL_QUESTION
}
@Controller('question')
export class QuestionController {

    constructor(private questionService: QuestionService) { }

    @Post('list')
    async list(@Body() data,@Res() res: Response): Promise<any> {
        console.log(data);
        const responseObj: ResponseObj = await this.questionService.list(data);
        //console.log(responseObj);
        res.status(responseObj.code).json(responseObj.data);
    }

    @Post('askQuestion')
    async create(@Body() askBody, @Res() res: Response): Promise<any> {
        const responseObj: ResponseObj = await this.questionService.askQuestion(askBody);
        res.status(responseObj.code).json(responseObj.data);
    }

    
    @Post('getRelatedQuestions')
    async getRelatedQuestions(@Body() askBody, @Res() res: Response): Promise<any> {
        const responseObj: ResponseObj = await this.questionService.getRelatedQuestions(askBody);
        res.status(responseObj.code).json(responseObj.data);
    }

    @Post('answerQuestion')
    async answerQuestion(@Body() answerBody, @Res() res: Response): Promise<any> {
        const responseObj: ResponseObj = await this.questionService.answerQuestion(answerBody);
        res.status(responseObj.code).json(responseObj.data);
    }

    @Post('saveForLater')
    async saveForLater(@Body() body, @Res() res: Response): Promise<any> {
        const responseObj: ResponseObj = await this.questionService.saveForLater(body.questionId);
        res.status(responseObj.code).json(responseObj.data);
    }

    @Post('archive')
    async archive(@Body() body, @Res() res: Response): Promise<any> {
        const responseObj: ResponseObj = await this.questionService.archive(body.questionId);
        res.status(responseObj.code).json(responseObj.data);
    }


    @Post('share')
    async share(@Body() body, @Res() res: Response): Promise<any> {
        const responseObj: ResponseObj = await this.questionService.share(body.questionId, body.userId);
        res.status(responseObj.code).json(responseObj.data);
    }


    @Post('comment/vote')
    async setUpvoteToComment(@Body() body, @Res() res: Response): Promise<any> {
        const responseObj: ResponseObj = await this.questionService.setVote(body);
        res.status(responseObj.code).json(responseObj.data);
    }

    @Delete('comment/vote')
    async removeUpvoteToComment(@Body() body, @Res() res: Response): Promise<any> {
        const responseObj: ResponseObj = await this.questionService.removeVote(body);
        res.status(responseObj.code).json(responseObj.data);
    }

    @Get('explorePreview/:id')
    async explorePreview(@Param('id') id: string, @Res() res: Response): Promise<any> {
        const responseObj: ResponseObj = await this.questionService.explorePreview(id);
        res.status(responseObj.code).json(responseObj.data);
    }

    @Get('getQuestionByName/:name')
    async getQuestionByName(@Param('name') name: string, @Res() res: Response): Promise<any> {
        const responseObj: ResponseObj = await this.questionService.getQuestionByName(name);
        res.status(responseObj.code).json(responseObj.data);
    }

    @Delete()
    async removeQuestion(@Body() body, @Res() res: Response,): Promise<any> {
        const responseObj: ResponseObj = await this.questionService.deleteQuestion(body);
        res.status(responseObj.code).json(responseObj.data);
    }

    @Put(':id')
    async editQuestion(@Body() body, @Res() res: Response, @Param('id') id: string): Promise<any> {
        const responseObj: ResponseObj = await this.questionService.update(body, id);
        res.status(responseObj.code).json(responseObj.data);
    }

    @Delete('answer')
    async removeAnswer(@Body() body, @Res() res: Response): Promise<any> {
        const responseObj: ResponseObj = await this.questionService.deleteAnswer(body);
        res.status(responseObj.code).json(responseObj.data);
    }

    @Post('getQuestionById')
    async getQuestionById(@Body() body, @Res() res: Response): Promise<any> {
        const responseObj: ResponseObj = await this.questionService.getQuestionById(body.questionId, body.userId);
        res.status(responseObj.code).json(responseObj.data);
    }
    @Get('interest/:id')
    async getInterestQuestionById(@Param('id') id: string, @Res() res: Response): Promise<any> {
        const responseObj: ResponseObj = await this.questionService.getInterestQuestionById(id);
        res.status(responseObj.code).json(responseObj.data);
    }

    @Post('basedOnInterest')
    async getBasedOnInterest(@Body() body, @Res() res: Response): Promise<any> {
        const responseObj: ResponseObj = await this.questionService.getBasedOnInterest(body.interestId, body.userId);
        res.status(responseObj.code).json(responseObj.data);
    }



}
