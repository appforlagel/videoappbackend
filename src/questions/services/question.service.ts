import { Injectable } from '@nestjs/common';
import { neode } from 'src/main';
import { ResponseObj } from 'src/utils/interfaces/ResponseObj';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/models/postgresEntities/UserEntity';
import { Repository } from 'typeorm';
import { Question } from 'src/models/postgresEntities/QuestionEntity';
import { Comment } from 'src/models/postgresEntities/CommentEntity';
import { VoteToComment, VoteType } from 'src/models/postgresEntities/VoteToCommentEntity';
import { NotificationService } from 'src/notification/notification.service';
import { FeedService } from 'src/feed/feed.service';
import { CloudinaryService } from 'src/cloudinary/cloudinary.service';
import { Inject } from '@nestjs/common';
import { AnswerService } from 'src/answer/answer.service';
@Injectable()
export class QuestionService {
    constructor(
        @InjectRepository(User)
        private usersRepository: Repository<User>,
        @InjectRepository(Question)
        private questionRepository: Repository<Question>,
        @InjectRepository(Comment)
        private commentRepository: Repository<Comment>,
        @InjectRepository(VoteToComment)
        private voteToCommentRepository: Repository<VoteToComment>,
        private notificationService: NotificationService,
        private feedService: FeedService,
        private _answerService: AnswerService,
        @Inject(CloudinaryService)
        private readonly _cloudinaryService: CloudinaryService
    ) { }

    async askQuestion(body): Promise<any> {
        if (body.type == 'USER_QUESTION') {
            return this._userQuestion(body);
        } else {
            return this._generalQuestion(body);
        }

    }

    async list(data): Promise<any> {
        let responseObj: ResponseObj;
         return await neode.writeCypher(`
            MATCH (q: Question ) return q`, {})
        .then(
            (res) => {
                const result = res.records.map((x) => {
                    const question = x.get('q').properties;
                    return question;
                });
                responseObj = { code: 200, data: result };
                return responseObj;
            }).catch(
            (err) => {
                console.log(err);
                const responseObj: ResponseObj = { code: 400, data: err };
                    return responseObj;
            }
        );
    }

    async update(questionInfo, id): Promise<any> {
        return await neode.find('Question', id).then(
            async (question: any) => {
                if (question) {
                    return await question.update({ "privacy": questionInfo.privacy, "text": questionInfo.text }).then(
                        async (questionRes) => {
                            const updatedUser = await questionRes.toJson();
                            const responseObj: ResponseObj = { code: 200, data: updatedUser };
                            return responseObj;
                        },
                    ).catch(
                        (err) => {
                            console.log(err);
                        }
                    );
                } else {
                    const responseObj: ResponseObj = { code: 404, data: 'The user is not found in the database' };
                    return responseObj;
                }
            },
        );
    }

    async answerQuestion(body): Promise<any> {
        let responseObj: ResponseObj;
        try {
            const question = await neode.find('Question', body.questionId);
            const user = await neode.find('User', body.userId);
            body.answer.createdAt = Date.now();
            const answer = await neode.create('Answer', body.answer);
            if (answer) {
                await question.relateTo(answer, 'answers');
                answer.relateTo(user, 'answerUser')
                question.update({ lastAnswer: body.answer.createdAt })
                this._removeQuestionNotificatinIfExist(body);
                this.notificationService.createAnswerNotification(user.get('id'), question.get('id'));
                responseObj = { code: 200, data: 'Answer created' };
            } else {
                responseObj = { code: 400, data: 'error creating answer' };
            }

            return responseObj;
        } catch (e) {
            console.log(e);
            responseObj = { code: 500, data: 'Server Error' };
            return responseObj;
        }



    }

    _removeQuestionNotificatinIfExist(body) {
        neode.writeCypher(`
            MATCH (u: User {id: $userId})-[:USER_NOTIFICATIONS]-> (n: Notification) -[:QUESTION_NOTIFY]-> (q: Question {id:$questionId}) detach delete n
        `, { userId: body.userId, questionId: body.questionId }).then(
            res => {

            }
        );
    }

    async _userQuestion(userQuestion) {
        let responseObj: ResponseObj;
        const userSend = await neode.find('User', userQuestion.userSenderId);
        const userReceived = await neode.find('User', userQuestion.userReceiverId);

        if (userSend && userReceived) {
            userQuestion.questionData.text = userQuestion.questionData.text.replace("?", "").trim();
            Object.keys(userQuestion.questionData).forEach((key) => (userQuestion.questionData[key] == null) && delete userQuestion.questionData[key]);
            userQuestion.questionData.createdAt = Date.now();
            const question = await neode.merge('Question', userQuestion.questionData).catch((err) => { console.log(err) });
            if (question) {
                try {
                    await userSend.relateTo(question, 'questionsAsked');
                    await question.relateTo(userReceived, 'userReceived');
                    this.notificationService.createDirectQuestionNotification(question.get('id'), userQuestion.userSenderId);
                    responseObj = { code: 200, data: 'question created' };
                    return responseObj;
                } catch (e) {
                    responseObj = { code: 500, data: { msg: 'Internal error creating relationship', data: e.toString() } };
                    return responseObj;
                }

            } else {
                responseObj = { code: 500, data: 'Error creating the question' };
                return responseObj;
            }


        } else {
            responseObj = { code: 404, data: 'The user is not found in the database' };
            return responseObj;
        }
    }

    async _generalQuestion(askBody) {

        let responseObj: ResponseObj;
        const userSend = await neode.find('User', askBody.userSenderId);
        const interest = await neode.find('Interest', askBody.interestId);

        if (userSend && interest) {
            askBody.questionData.text = askBody.questionData.text.replace("?", "").trim();
            Object.keys(askBody.questionData).forEach((key) => (askBody.questionData[key] == null) && delete askBody.questionData[key]);
            const questionId = await this._isInterestQuestionExist(askBody.questionData.text, askBody.userSenderId, askBody.interestId);
            if (questionId.length < 1) {
                const question = await neode.merge('Question', askBody.questionData);
                if (question) {
                    try {
                        await userSend.relateTo(question, 'questionsAsked');
                        await question.relateTo(interest, 'interestReceived');
                        this.notificationService.createAskInterestQuestionNotification(askBody.userSenderId, askBody.interestId, question.get('id'))
                        responseObj = { code: 200, data: question.get('id') };
                        return responseObj;
                    } catch (e) {
                        responseObj = { code: 500, data: { msg: 'Internal error creating relationship', data: e.toString() } };
                        return responseObj;
                    }

                } else {
                    responseObj = { code: 500, data: 'Error creating the question' };
                    return responseObj;
                }
            } else {
                const questionDid = await this._checkIfUserDidTheQuestion(questionId, askBody.userSenderId);
                if (questionDid) {
                    responseObj = { code: 422, data: questionId };
                } else {
                    responseObj = { code: 200, data: 'question created' };
                }
                return responseObj;
            }


        } else {
            responseObj = { code: 404, data: 'The user or interest not found in the database' };
            return responseObj;
        }

    }

    async _getRelatedUserQuestions(question: string, receiverId: string) {

        return await neode.cypher(`
            MATCH  (q:Question)-[:QUESTIONS_RECEIVED]->(receiverU: User {id:$receiverId}) 
            WHERE replace(q.text, " ", "") =~ ('.*' + $question +'.*') 
            OPTIONAL MATCH (a: Answer)<-[:ANSWER]-(q)
            return q, a
        `, { receiverId: receiverId, question: question.toLowerCase().split(" ").join("") }).then(
            (res) => {
                const result = [];
                if (res.records.length > 0) {
                    res.records.forEach((x) => {
                        const aux = x.get('q').properties;
                        if (x.get('a') != null) {
                            aux.answer = x.get('a').properties;
                        }
                        result.push(aux);
                    });
                    return result;
                } else {
                    return result;
                }
            });

    }

    async _getRelatedInterestQuestions(question: string, receiverId: string) {

        return await neode.cypher(`
            MATCH  (q:Question)-[:QUESTIONS_RECEIVED]->(receiverI: Interest {id:$receiverId}) 
            WHERE replace(q.text, " ", "") =~ ('.*' + $question +'.*') 
            OPTIONAL MATCH (a: Answer)<-[:ANSWER]-(q)
            with collect(a) as answers, q
            return q {.* , answers}
        `, { receiverId: receiverId, question: question.toLowerCase().split(" ").join("") }).then(
            (res) => {
                const result = [];
                if (res.records.length > 0) {
                    res.records.forEach((x) => {
                        const aux = x.get('q');

                        result.push(aux);
                    });
                    return result;
                } else {
                    return result;
                }
            });

    }

    async getRelatedQuestions(body) {
        let questions;
        if (body.userReceivedId != null) {
            questions = await this._getRelatedUserQuestions(body.text, body.userReceivedId);
        } else {
            questions = await this._getRelatedInterestQuestions(body.text, body.interestReceivedId);
        }

        const responseObj: ResponseObj = { code: 200, data: questions };
        return responseObj;
    }

    async _isInterestQuestionExist(question: string, senderId: string, interestId: string) {

        return await neode.writeCypher(`
            MATCH (senderU:User {id:$senderId})-[:QUESTIONS_ASKED]-> 
            (q:Question)-[:QUESTIONS_RECEIVED]->
            (i: Interest {id:$interestId}) WHERE replace(q.text, " ", "") =replace($question, " ", "") return q
        `, { senderId: senderId, interestId: interestId, question: question }).then(
            (res) => {
                if (res.records.length > 0) {
                    return res.records[0].get('q').properties.id;
                } else {
                    return '';
                }
            });

    }

    async _checkIfUserDidTheQuestion(questionId, userId) {
        const myQuestion = await neode.writeCypher(`
            MATCH(u: User {id: $userId})-[r:QUESTIONS_ASKED]-> 
            (q: Question {id: $questionId}) return r
        `, { questionId: questionId, userId: userId }).then(
            (res) => {
                if (res.records.length > 0) {
                    return true;
                } else {
                    return false;
                }
            });
        if (!myQuestion) {
            const user = await neode.find('User', userId);
            const question = await neode.find('Question', questionId);
            await user.relateTo(question, 'questionsAsked');
            return false;
        }
        return true;

    }

    async saveForLater(questionId): Promise<any> {
        let responseObj: ResponseObj;
        return await neode.writeCypher(`
            MATCH(q: Question {id: $questionId}) set q.saveForLater = true return q
        `, { questionId: questionId, }).then(
            (res) => {
                if (res.records.length > 0) {
                    responseObj = { code: 200, data: true };
                    return responseObj;
                } else {
                    responseObj = { code: 400, data: false };
                    return responseObj;
                }
            });
    }

    async archive(questionId): Promise<any> {
        let responseObj: ResponseObj;
        return await neode.writeCypher(`
            MATCH(q: Question {id: $questionId}) set q.archived = true return q
        `, { questionId: questionId, }).then(
            (res) => {
                if (res.records.length > 0) {
                    responseObj = { code: 200, data: true };
                    return responseObj;
                } else {
                    responseObj = { code: 400, data: false };
                    return responseObj;
                }
            });
    }


    async share(questionId, userId): Promise<any> {
        let responseObj: ResponseObj;

        try {
            const user = await neode.find('User', userId);
            const question = await neode.find('Question', questionId);

            if (user && question) {
                await user.relateTo(question, 'questionsShare');
                responseObj = { code: 200, data: 'Success' };
                return responseObj;
            } else {
                responseObj = { code: 400, data: 'User or question not found' };
                return responseObj;
            }

        } catch (e) {
            console.log(e);
            responseObj = { code: 500, data: e };
            return responseObj;
        }

    }

    async setVote(body) {
        let responseObj: ResponseObj;

        let user = await this.usersRepository.findOne({ where: { userId: body.userId } });
        const comment = await this.commentRepository.findOne(body.commentId);

        if (!user) {
            user = new User();
            user.userId = body.userId;
            user = await this.usersRepository.save(user);
        }

        if (comment) {
            let voteToComment = await this.voteToCommentRepository.findOne({ userId: user.id, commentId: comment.id });
            if (!voteToComment) {
                voteToComment = new VoteToComment();
                voteToComment.date = new Date();
                voteToComment.user = user;
                voteToComment.comment = comment;
            }
            voteToComment.voteType = body.voteType;
            return this.voteToCommentRepository.save(voteToComment).then(
                res => {
                    responseObj = { code: 200, data: res };
                    return responseObj;
                },
                err => {
                    responseObj = { code: 500, data: 'Internal server Error' + err.message };
                    return responseObj;
                },
            );
        } else {
            return responseObj = { code: 400, data: 'comment not found ' };
        }

    }

    async removeVote(body) {
        let responseObj: ResponseObj;
        const user = await this.usersRepository.findOne({ where: { userId: body.userId } });
        const voteToComment = await this.voteToCommentRepository.findOne({ where: { userId: user.id, commentId: body.commentId } })

        if (voteToComment) {
            return this.voteToCommentRepository.delete(voteToComment).then(
                res => {
                    responseObj = { code: 200, data: res };
                    return responseObj;
                },
                err => {
                    responseObj = { code: 500, data: 'Internal server Error' + err.message };
                    return responseObj;
                },
            );
        } else {
            return responseObj = { code: 400, data: 'comment not found ' };
        }

    }

    async explorePreview(userId) {
        let responseObj: ResponseObj;
        const result = [];
        return await neode.writeCypher(`
            MATCH(u: User {id: $userId})-[:INTERESTED_IN]->(i: Interest) return i
        `, { userId: userId }).then(
            async (res) => {
                res.records.forEach((x) => {
                    const interest = x.get('i').properties;
                    interest.questions = [];
                    result.push(interest);
                })
                return await neode.writeCypher(`
                    MATCH(u:User {id: $userId}) -[:INTERESTED_IN]-> (i: Interest)
                    OPTIONAL MATCH (i)<-[:QUESTIONS_RECEIVED]- (q:Question)
                    return   q,  i
                `, { userId: userId }).then(
                    (res) => {
                        res.records.forEach((x) => {
                            if (x.get('q')) {
                                const index = result.findIndex((interest) => {
                                    return interest.id == x.get('i').properties.id;
                                })
                                result[index].questions.push(x.get('q').properties);
                            }
                        })
                        responseObj = { code: 200, data: result };
                        return responseObj;
                    });
            });
    }

    async getQuestionByName(name) {
        let responseObj: ResponseObj;
        return await neode.writeCypher(`
            MATCH (q: Question )-[:QUESTIONS_RECEIVED]->(i:Interest) where q.text contains $subString return q, i
        `, { subString: name }).then(
            (res) => {
                const result = res.records.map((x) => {
                    const question = x.get('q').properties;
                    question.interest = x.get('i').properties;
                    return question;
                });
                responseObj = { code: 200, data: result };
                return responseObj;
            });
    }


    async deleteQuestion(body) {
        let resposeObj: ResponseObj;
        return neode.readCypher(`
            MATCH (q:Question {id: $questionId}) return exists((q)-[:ANSWER]->(:Answer)) as hasAnswer
        `, { questionId: body.questionId }).then(
            res => {
                if (!res.records[0].get('hasAnswer')) {
                    return neode.writeCypher(
                        `
                        MATCH (q:Question {id: $questionId}) 
                        OPTIONAL MATCH (q)<-[:QUESTION_NOTIFY]-(n:Notification) 
                        detach delete q ,n `,
                        { questionId: body.questionId }
                    ).then(
                        res => {
                            resposeObj = { code: 200, data: 'Can delete' };
                            return resposeObj;
                        },
                        err => {
                            resposeObj = { code: 500, data: 'Error deleting the question' };
                            return resposeObj;
                        }
                    )

                } else {
                    console.log('cant delete');
                    resposeObj = { code: 400, data: 'Cant Delete' };
                    return resposeObj;
                }
            }
        )
    }

    async deleteAnswer(body) {
        let resposeObj: ResponseObj;
        return neode.writeCypher(
            `MATCH (a:Answer {id: $answerId}) 
             OPTIONAL MATCH (a)<-[:ANSWER_NOTIFY]-(n:Notification) 
             with properties(a) as answer, a,n detach delete a , n  return answer`, { answerId: body.answerId }
        ).then(
            async res => {
                const answer = res.records[0].get('answer');
                let result;
                if (answer.cloudinaryPublicId != null) {
                    result = await this._cloudinaryService.delete(answer.cloudinaryPublicId);
                }
                resposeObj = { code: 200, data: 'Can delete' };
                return resposeObj;
            },
            err => {
                console.log(err);
                resposeObj = { code: 500, data: 'Error deleting the question' };
                return resposeObj;
            }
        )
    }

    async getQuestionById(questionId, userId) {
        let responseObj: ResponseObj;
        try {
            return await neode.writeCypher(`
                MATCH (u: User {id:$userId}), (q: Question {id:$questionId})
                MATCH (userAsk: User) -[: QUESTIONS_ASKED]->(q)
                OPTIONAL MATCH (q)-[an:ANSWER]-> (a:Answer)-[:ANSWER_USER]->(au: User)
                OPTIONAL MATCH (q)<-[sh:SHARE ]-(:User) 
                OPTIONAL MATCH (a)<-[like:LIKE]-(:User)
                OPTIONAL MATCH (a)<-[view:VIEW]-(:User)
                OPTIONAL MATCH (q)-[:QUESTIONS_RECEIVED]->(iq: Interest)
                WITH 
                toFloat(count(like)) as likes , 
                toFloat(sum(view.views)) as views,
                exists((u)-[:LIKE]->(a)) as userLike, 
                exists((u)-[:SHARE]->(q)) as userShare,
                u, q,a, sh, like,iq,au,userAsk
                order by likes desc
                return  q {.*, userShare, shares: toFloat(count(distinct sh)),interest:properties(iq), userAsked: properties(userAsk)},
                    collect(distinct a {.*, userLike , likes, views, userAnswered: properties(au), views}) as answers 
                    order by q.lastAnswer desc 
            `, { questionId: questionId, userId: userId }).then(async (res) => {
                if (res.records.length > 0) {
                    const result = [];
                    res.records.forEach((x) => {
                        const aux = x.get('q');
                        aux.answers = x.get('answers');
                        result.push(aux);
                    })
                    responseObj = {
                        code: 200,
                        data: result[0],
                    };
                    return responseObj
                } else {
                    responseObj = {
                        code: 400,
                        data: 'Question not found',
                    };
                    return responseObj
                }

            }).catch((err) => {
                console.log(err);
                responseObj = { code: 400, data: err };
                return responseObj
            });
        } catch (e) {
            console.log(e);
            responseObj = { code: 500, data: e };
            return responseObj
        }

    }

    async getInterestQuestionById(questionId) {
        let responseObj: ResponseObj;
        try {
            return await neode.writeCypher(`
                MAtch  (userAsk: User) -[: QUESTIONS_ASKED]->(q: Question {id:$questionId})-[:QUESTIONS_RECEIVED] -> (i:Interest )
                OPTIONAL MATCH (q)-[an:ANSWER]-> (a:Answer)-[:ANSWER_USER]->(au: User)
                OPTIONAL MATCH (q)<-[sh:SHARE ]-(:User) 
                OPTIONAL MATCH (a)<-[up:UPVOTE]-(:User)
                OPTIONAL MATCH (a)<-[do:DOWNVOTE ]-(:User)
                with 
                toFloat(count(sh)) as shares,
                toFloat(count(up)) as upVotes , 
                toFloat(count(do)) as downVotes, 
                a, q, properties(au) as userAnswered ,userAsk {.*} as userAsked, properties(i) as interest
                return DISTINCT  q {.*, shares, userAsked, interest }, collect(a {.*,upVotes,downVotes,userAnswered}) as answers
            `, { questionId: questionId, }).then(async (res) => {
                if (res.records.length > 0) {
                    const result = [];
                    res.records.forEach((x) => {
                        const aux = x.get('q');
                        aux.answers = x.get('answers');
                        result.push(aux);
                    })
                    responseObj = {
                        code: 200,
                        data: result[0],
                    };
                    return responseObj
                } else {
                    responseObj = {
                        code: 400,
                        data: 'Question not found',
                    };
                    return responseObj
                }

            }).catch((err) => {
                console.log(err);
                responseObj = { code: 400, data: err };
                return responseObj
            });
        } catch (e) {
            console.log(e);
            responseObj = { code: 500, data: e };
            return responseObj
        }

    }

    async getBasedOnInterest(interestId, userId) {
        let responseObj: ResponseObj;
        try {
            return await neode.writeCypher(`
                MATCH (q: Question)-[:QUESTIONS_RECEIVED]->(i: Interest {id: $interestId})
                MATCH (userAsk: User) -[: QUESTIONS_ASKED]->(q)
                OPTIONAL MATCH (q)-[an:ANSWER]-> (a:Answer)-[:ANSWER_USER]->(au: User)
                MATCH (u: User {id:$userId}) 
                OPTIONAL MATCH (q)<-[sh:SHARE]-(:User)
                OPTIONAL MATCH (a)<-[like:LIKE]-(:User)
                OPTIONAL MATCH (a)<-[view:VIEW]-(:User) 
                WITH 
                toFloat(count( like)) as likes , 
                toFloat(sum(view.views)) as views,
                exists((u)-[:LIKE]->(a)) as userLike, 
                exists((u)-[:SHARE]->(q)) as userShare,
                u, i, q,a, sh, like,au,userAsk
                order by likes desc
                return  q {.*, userShare, shares: toFloat(count(distinct sh)),interest:properties(i), userAsked: properties(userAsk)},
                    collect(distinct a {.*, userLike , likes, views, userAnswered: properties(au), views}) as answers 
                    order by q.lastAnswer desc limit 10
            `, { interestId: interestId, userId: userId }).then(async (res) => {
                if (res.records.length > 0) {
                    const result = [];
                    for (let i = 0; i < res.records.length; i++) {
                        const aux = res.records[i].get('q');
                        aux.answers = res.records[i].get('answers')
                        if (aux.answers != null && aux.answers.length > 0) {
                            const responseObj: ResponseObj = await this._answerService.getCommets(aux.answers[0].id);
                            if (responseObj.code == 200) {
                                aux.answers[0].comments = responseObj.data;
                            }
                        }
                        result.push(aux);
                    }

                    responseObj = {
                        code: 200,
                        data: result
                    };
                    return responseObj
                } else {
                    responseObj = {
                        code: 422,
                        data: 'Questions not found',
                    };
                    return responseObj
                }

            }).catch((err) => {
                console.log(err);
                responseObj = { code: 400, data: err };
                return responseObj
            });
        } catch (e) {
            console.log(e);
            responseObj = { code: 500, data: e };
            return responseObj
        }
    }



}
