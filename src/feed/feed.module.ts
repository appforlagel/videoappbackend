import { Module } from '@nestjs/common';
import { AnswerService } from 'src/answer/answer.service';
import { FeedController } from './feed.controller';
import { FeedService } from './feed.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Answer } from 'src/models/postgresEntities/AnswerEntity';
import { Comment } from 'src/models/postgresEntities/CommentEntity';
import { User } from 'src/models/postgresEntities/UserEntity';
import { VoteToComment } from 'src/models/postgresEntities/VoteToCommentEntity';

@Module({
  imports: [
    TypeOrmModule.forFeature([User, Comment, Answer, VoteToComment]),
  ],
  controllers: [FeedController],
  providers: [FeedService, AnswerService]
})
export class FeedModule { }
