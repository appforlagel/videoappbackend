import { Injectable } from '@nestjs/common';
import { AnswerService } from 'src/answer/answer.service';
import { neode } from 'src/main';
import { Answer } from 'src/models/postgresEntities/AnswerEntity';
import { ResponseObj } from 'src/utils/interfaces/ResponseObj';

@Injectable()
export class FeedService {


    constructor(private _answerService: AnswerService) { }

    async getFeed(body): Promise<any> {
        let responseObj: ResponseObj;
        
        //temp hack to reduce feed results to 5
        //limit should be set to 5 in client side
        let limit = 5;
        let skip = body.skip;
        if(skip % 10 == 0) {
            skip /= 2;
        }

        try {
            return await neode.writeCypher(`
                MATCH (u: User {id:$userId})-[:INTERESTED_IN]->(i:Interest)
                WITH u, i, collect(i.id) as interests
                MATCH (au: User)<-[:ANSWER_USER]-(a:Answer)<-[an:ANSWER]-(q: Question)<-[:QUESTIONS_ASKED]-(userAsk:User)
                MATCH (q)-[:QUESTIONS_RECEIVED]->(iq: Interest)
                WITH u, i, q, a, iq, au, userAsk
                WHERE iq.id IN interests
                OPTIONAL MATCH (q)<-[share:SHARE]-(:User)
                OPTIONAL MATCH (a)<-[like:LIKE]-(:User)
                OPTIONAL MATCH (a)<-[view:VIEW]-(:User) 
                WITH 
                toFloat(count(like)) as likes , 
                toFloat(sum(view.views)) as views,
                exists((u)-[:LIKE]->(a)) as userLike, 
                exists((u)-[:SHARE]->(q)) as userShare,
                u, i, q, a, iq, au, userAsk, share, like
                return  q {.*, userShare, shares: toFloat(count(distinct share)),interest:properties(iq), userAsked: properties(userAsk)},
                    collect(distinct a {.*, userLike , likes, views, userAnswered: properties(au), views}) as answers 
                    order by q.lastAnswer desc 
                    skip toInteger($skip) limit toInteger($limit)
            `, { userId: body.userId, skip: body.skip, limit: body.limit }).then(async (res) => {

                responseObj = {
                    code: 200,
                    data: await this._prepareFeedData(res.records)
                };
                return responseObj

            }).catch((err) => {
                console.log(err);
                responseObj = { code: 400, data: err };
                return responseObj
            });
        } catch (e) {
            console.log(e);
            responseObj = { code: 500, data: e };
            return responseObj
        }

    }

    async _prepareFeedData(records) {
        const result = [];
        for (let i = 0; i < records.length; i++) {
            const question = records[i].get('q');
            question.answers = records[i].get('answers');
            const responseObj: ResponseObj = await this._answerService.getCommets(question.answers[0].id);
            if (responseObj.code == 200) {
                question.answers[0].comments = responseObj.data;
            }

            result.push(question);
        }
        return result
    }

}
