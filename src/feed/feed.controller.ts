import { Controller, Post, Body, Res } from '@nestjs/common';
import { ResponseObj } from 'src/utils/interfaces/ResponseObj';
import { FeedService } from './feed.service';
import { Response } from 'express';
@Controller('feed')
export class FeedController {

    constructor(private feedService: FeedService) { }

    @Post('get')
    async getFeed(@Body() body, @Res() res: Response): Promise<any> {
        const responseObj: ResponseObj = await this.feedService.getFeed(body);
        res.status(responseObj.code).json(responseObj.data);
    }
}
