export interface ResponseObj {
    code: number;
    data?: any;
    error?: any;
}