import { Controller, Post, Body, Res, Get, Param, Delete } from '@nestjs/common';

import { ResponseObj } from 'src/utils/interfaces/ResponseObj';
import { Response } from 'express';
import { AnswerService } from './answer.service';
@Controller('answer')
export class AnswerController {

    constructor(private _answerService: AnswerService) { }

    @Post('list')
    async list(@Body() data,@Res() res: Response): Promise<any> {
        console.log(data);
        const responseObj: ResponseObj = await this._answerService.list(data);
        //console.log(responseObj);
        res.status(responseObj.code).json(responseObj.data);
    }

    @Post('like')
    async like(@Body() body, @Res() res: Response): Promise<any> {
        const responseObj: ResponseObj = await this._answerService.like(body.answerId, body.userId);
        res.status(responseObj.code).json(responseObj.data);
    }


    @Post('dislike')
    async dislike(@Body() body, @Res() res: Response): Promise<any> {
        const responseObj: ResponseObj = await this._answerService.dislike(body.answerId, body.userId);
        res.status(responseObj.code).json(responseObj.data);
    }

    @Get('comment/:id')
    async getComments(@Param('id') id: string, @Res() res: Response): Promise<any> {
        const responseObj: ResponseObj = await this._answerService.getCommets(id);
        res.status(responseObj.code).json(responseObj.data);
    }

    @Post('comment')
    async sendComment(@Body() body, @Res() res: Response): Promise<any> {
        const responseObj: ResponseObj = await this._answerService.createComment(body);
        res.status(responseObj.code).json(responseObj.data);
    }

    @Get('deleteComment/:id')
    async deleteComment(@Param('id') id: number, @Res() res: Response): Promise<any> {
        const responseObj: ResponseObj = await this._answerService.deleteComment(id);
        res.status(responseObj.code).json(responseObj.data);
    }

    @Post('updatecomment')
    async updateComment(@Body() body, @Res() res: Response): Promise<any> {
        const responseObj: ResponseObj = await this._answerService.updateComment(body);
        res.status(responseObj.code).json(responseObj.data);
    }

    @Post('addView')
    async addView(@Body() body, @Res() res: Response): Promise<any> {
        const responseObj: ResponseObj = await this._answerService.addView(body);
        res.status(responseObj.code).json(responseObj.data);
    }


}
