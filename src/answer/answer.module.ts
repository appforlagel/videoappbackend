import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Answer } from 'src/models/postgresEntities/AnswerEntity';
import { Comment } from 'src/models/postgresEntities/CommentEntity';
import { User } from 'src/models/postgresEntities/UserEntity';
import { VoteToComment } from 'src/models/postgresEntities/VoteToCommentEntity';
import { AnswerController } from './answer.controller';
import { AnswerService } from './answer.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([User, Comment, Answer, VoteToComment]),
  ],
  controllers: [AnswerController],
  providers: [AnswerService]
})
export class AnswerModule { }
