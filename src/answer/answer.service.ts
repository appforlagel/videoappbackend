import { Comment } from './../models/postgresEntities/CommentEntity';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { neode } from 'src/main';
import { Answer } from 'src/models/postgresEntities/AnswerEntity';
import { Question } from 'src/models/postgresEntities/QuestionEntity';
import { User } from 'src/models/postgresEntities/UserEntity';
import { VoteToComment, VoteType } from 'src/models/postgresEntities/VoteToCommentEntity';
import { NotificationService } from 'src/notification/notification.service';
import { ResponseObj } from 'src/utils/interfaces/ResponseObj';
import { Repository } from 'typeorm';
import { use } from 'passport';

@Injectable()
export class AnswerService {

    constructor(
        @InjectRepository(User)
        private usersRepository: Repository<User>,
        @InjectRepository(Answer)
        private answerRepository: Repository<Answer>,
        @InjectRepository(Comment)
        private commentRepository: Repository<Comment>,
        @InjectRepository(VoteToComment)
        private voteToCommentRepository: Repository<VoteToComment>,
        private notificationService: NotificationService
    ) { }

    async list(data): Promise<any> {
        let responseObj: ResponseObj;
         return await neode.writeCypher(`
            MATCH (a: Answer ) return a`, {})
        .then(
            (res) => {
                const result = res.records.map((x) => {
                    const users = x.get('a').properties;
                    return users;
                });
                responseObj = { code: 200, data: result };
                return responseObj;
            }).catch(
            (err) => {
                console.log(err);
                const responseObj: ResponseObj = { code: 400, data: err };
                    return responseObj;
            }
        );
    }

    async upVote(answerId, userId): Promise<any> {
        let responseObj: ResponseObj;


        try {
            return await neode.writeCypher(`
            MATCH (u:User {id: $userId})-[vote :DOWNVOTE]->(a: Answer {id: $answerId}) detach delete vote
            `, { answerId: answerId, userId: userId }).then(
                async (res) => {
                    return await neode.writeCypher(`
                        MATCH(u:User {id: $userId}), (a: Answer {id: $answerId}) 
                        MERGE (u)-[vote:UPVOTE]->(a) return vote
                    `, { answerId: answerId, userId: userId }).then(
                        (res) => {

                            if (res.records.length > 0) {
                                responseObj = { code: 200, data: true };
                                return responseObj;
                            } else {
                                responseObj = { code: 400, data: false };
                                return responseObj;
                            }
                        });
                });

        } catch (e) {
            responseObj = { code: 500, data: e };
            console.log(e);
            return responseObj;
        }

    }

    async downVote(answerId, userId): Promise<any> {
        let responseObj: ResponseObj;


        try {
            return await neode.writeCypher(`
            MATCH (u:User {id: $userId})-[vote :UPVOTE]->(a: Answer {id: $answerId}) detach delete vote
            `, { answerId: answerId, userId: userId }).then(
                async (res) => {
                    return await neode.writeCypher(`
                        MATCH(u:User {id: $userId}), (a: Answer {id: $answerId}) 
                        MERGE (u)-[vote :DOWNVOTE]->(a) return vote
                    `, { answerId: answerId, userId: userId }).then(
                        (res) => {

                            if (res.records.length > 0) {
                                responseObj = { code: 200, data: true };
                                return responseObj;
                            } else {
                                responseObj = { code: 400, data: false };
                                return responseObj;
                            }
                        });
                });

        } catch (e) {
            responseObj = { code: 500, data: e };
            return responseObj;
        }

    }

    async like(answerId, userId): Promise<any> {
        let responseObj: ResponseObj;
        return await neode.writeCypher(`
            MATCH(u:User {id: $userId}), (a: Answer {id: $answerId}) 
            MERGE (u)-[like:LIKE]->(a) return like
        `, { answerId: answerId, userId: userId }).then(
            (res) => {
                this.notificationService.createLikeNotification(userId, answerId);
                if (res.records.length > 0) {
                    responseObj = { code: 200, data: true };
                    return responseObj;
                } else {
                    responseObj = { code: 400, data: false };
                    return responseObj;
                }
            });
    }

    async dislike(answerId, userId): Promise<any> {

        let responseObj: ResponseObj;
        return await neode.writeCypher(`
            MATCH (u:User {id: $userId})-[like :LIKE]->(a: Answer {id: $answerId}) 
            OPTIONAL MATCH (a)<-[:ANSWER_NOTIFY]-(n:Notification {type:"like"}) 
            set n.likeCount = n.likeCount - 1
            detach delete like
        `, { answerId: answerId, userId: userId }).then(
            (res) => {
                responseObj = { code: 200, data: true };
                return responseObj;
            });
    }

    async createComment(body): Promise<any> {
        let responseObj: ResponseObj;

        let user = await this.usersRepository.findOne({ where: { userId: body.userId } });
        let answer = await this.answerRepository.findOne({ where: { answerId: body.answerId } });


        if (!user) {
            user = new User();
            user.userId = body.userId;
            user = await this.usersRepository.save(user);
        }
        if (!answer) {
            answer = new Answer();
            answer.answerId = body.answerId;
            answer = await this.answerRepository.save(answer);
        }

        const comment = new Comment();
        comment.user = user;
        comment.body = body.commentData.body;
        comment.date = new Date(Date.now());

        if (body.parentCommentId != -1) {
            const parentComment = await this.commentRepository.findOne({ where: { id: body.parentCommentId, } });
            comment.parentComment = parentComment;
        } else {
            comment.parentAnswer = answer;
        }


        return this.commentRepository.save(comment).then(
            async res => {
                const jsonResponse = await this.getJsonDataFromComments([comment]);
                responseObj = { code: 200, data: jsonResponse[0] };
                this.notificationService.createCommentNotification(body.userId, body.answerId, comment);
                return responseObj;
            },
            err => {
                responseObj = { code: 500, data: 'Internal server Error' + err.message };
                return responseObj;
            },
        )
    }


    async updateComment(body: Comment): Promise<any> {
        let responseObj: ResponseObj;
        return  this.commentRepository.update(body.id, body).then(
            async res => {
                responseObj = { code: 200, data: "1"};
                return responseObj;
            },
            err => {
                responseObj = { code: 500, data: 'Internal server Error' + err.message };
                return responseObj;
            },
        )
    }

    async deleteComment(id: number) {
        let responseObj: ResponseObj;
        return await this.commentRepository.delete(id).then(
            async res => {
                responseObj = { code: 200, data: "1"};
                return responseObj;
            },
            err => {
                responseObj = { code: 500, data: 'Internal server Error' + err.message };
                return responseObj;
            },
        );
      }

    async getCommets(answerId: string): Promise<any> {
        let responseObj: ResponseObj;
        const answer = await this.answerRepository.findOne({
            where: { answerId: answerId },
            relations: [
                'comments', 'comments.user', 'comments.votes', 'comments.votes.user',
                'comments.answers', 'comments.answers.votes', 'comments.answers.user', 'comments.answers.votes.user', 'comments.answers.parentComment'
            ],
            order: {

            }
        });

        if (answer) {
            const comments = answer.comments;
            comments.sort((comment1, comment2) => {
                const date1 = new Date(comment1.date);
                const date2 = new Date(comment2.date);
                if (date1 > date2) {
                    return 1;
                } else if (date1 < date2) {
                    return -1;
                } else {
                    return 0;
                }

            })
            if (answer) {
                const jsonResult = await this.getJsonDataFromComments(comments);
                responseObj = { code: 200, data: jsonResult };
            } else {
                responseObj = { code: 500, data: 'Internal server Error' };
            }

        } else {
            responseObj = { code: 400, data: 'No comments' };
        }

        return responseObj;
    }

    async getJsonDataFromComments(comments: Comment[]){
        const result = [];
        try {

            if (comments) {
                for (let i = 0; i < comments.length; i++) {
                    let user = await neode.find('User', comments[i].user.userId);
                    result.push({
                        id: comments[i].id,
                        date: new Date(comments[i].date).getTime(),
                        body: comments[i].body,
                        user: await this.userparse(user) ,
                        answers: await this.getJsonDataFromComments(comments[i].answers),
                        upVotes: await this.calculeVotesFromList(comments[i].votes, VoteType.UPVOTE),
                        downVotes: await this.calculeVotesFromList(comments[i].votes, VoteType.DOWNVOTE),
                        parentComment: comments[i].parentComment ? { id: comments[i].parentComment.id } : null
                    });
                }
            }

        } catch (e) {
            console.log('loool');
            console.log(e)
        }
        return result;
    }

    userparse(user){
        let v1 = new Map(user["_properties"]); 
        let v = {
            "id" : v1.get("id"),
            "firstname" : v1.get("firstname"),
            "lastname" : v1.get("lastname"),
            "username" : v1.get("username"),
            "email" : v1.get("email"),
            "googleId" : v1.get("googleId"),
            "facebookId" : v1.get("facebookId"),                    
            "avatar" : v1.get("avatar"),
            "bio" : v1.get("bio"),
            "job" : v1.get("job"),
            "education" : v1.get("education"),
            "latitude" : v1.get("latitude"),
            "longitude" : v1.get("longitude"),
            "live" : v1.get("live"),
            "privateProfile" : v1.get("privateProfile"),
            "notificationEnable" : v1.get("notificationEnable"),
            "timestamp" : v1.get("timestamp"),
            "questionsReceived" : v1.get("questionsReceived"),
            "questionsAnswered" : v1.get("questionsAnswered"),
            "questionsAsked" : v1.get("questionsAsked"),
            "questionsShared" : v1.get("iquestionsSharedd"),
            "interests" : v1.get("interests"),
            "notifications" : v1.get("notifications"),
            "following" : v1.get("following"),
            "followers" : v1.get("followers"),
            "followNotification" : v1.get("followNotification"),
            "questionForYouNotification" : v1.get("questionForYouNotification"),
            "directMessagesNotification" : v1.get("directMessagesNotification"),
            "likeNotification" : v1.get("likeNotification"),
            "commentNotification" : v1.get("commentNotification"),
            "answerNotification" : v1.get("answerNotification"),
            "interestQuestionNotification" : v1.get("interestQuestionNotification"),
            "blockedUsers" : v1.get("blockedUsers"),
            "interestQuestblockedUsersForionNotification" : v1.get("interestQuestblockedUsersForionNotification"),
        }
        return v;
    }

    async calculeVotesFromList(voteToComment: VoteToComment[], voteType: VoteType) {
        const result = [];
        if (voteToComment) {
            for (let i = 0; i < voteToComment.length; i++) {
                if (voteType == voteToComment[i].voteType) {
                    const jsonResponse = {
                        id: voteToComment[i].user.userId
                    }
                    result.push(jsonResponse);
                }
            }
        }
        return result;
    }

    async addView(body) {
        let responseObj: ResponseObj;
        return await neode.writeCypher(`
            MATCH (u:User {id: $userId}),(a: Answer {id: $answerId}) 
            MERGE (u)-[v :VIEW ]->(a)
            ON CREATE SET v.views = 1
            ON MATCH SET v.views = v.views + 1
            return v
        `, { answerId: body.answerId, userId: body.userId }).then(
            async (res) => {
                if (res.records.length > 0) {
                    responseObj = { code: 200, data: true };
                    return responseObj;
                } else {
                    responseObj = { code: 400, data: false };
                    return responseObj;
                }
            });

    }

    async getCommetsCount(answerId: string): Promise<any> {
        const answer = await this.answerRepository.findOne({
            where: { answerId: answerId },
            relations: [
                'comments'
            ],
            order: {

            }
        });

        if (answer) {
            return answer.comments.length;

        } else {
            return 0;
        }

    }


}
