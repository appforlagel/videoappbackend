import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { neode } from 'src/main';
import { ResponseObj } from 'src/utils/interfaces/ResponseObj';
import { createHmac } from 'crypto';
import { EjabberdController } from '../chat/controllers/ejabberd.controller'

@Injectable()
export class AuthService {
    constructor(
        private jwtService: JwtService,
        private ejabberdController: EjabberdController
    ) { }

    async validateUser(username: string, pass: string): Promise<any> {
        // const user = await this.usersService.findOne(username);
        // if (user && user.password === pass) {
        //     const { password, ...result } = user;
        //     return result;
        // }
        // return null;
    }

    async loginDb(params: any, email: string) {

        return await neode.first('User', { email: email }, 1).then(
            async (res: any) => {
                let responseObj: ResponseObj;
                if (res) {
                    const user = await res.toJson();
                    try {
                        await res.update(params);
                    } catch (e) { console.log(e); }
                    //Ejabberd register
                    const key = Buffer.from('my super secret key', 'utf8');
                    const password = createHmac('sha512', key).update(user.id).digest('base64');
                    const xmppRegister = await this.ejabberdController.xmppRegister({
                        user: user.id,
                        host: 'videoapp',
                        password: password
                    })
                    console.log(xmppRegister)
                    if (xmppRegister.statusCode == 200 || xmppRegister.statusCode == 409) { 
                        //Register success or already exists -> OK
                        const payload = {
                            email: user.email,
                            userId: user.id,
                            name: user.firstname + ' ' + user.lastname,
                        };
                        const token = this.jwtService.sign(payload);
                        const responseData = {
                            authToken: token,
                            email: user.email,
                            firstname: user.firstname,
                            lastname: user.lastname,
                            userId: user.id,
                            xmppPassword: password
                        };
                        responseObj = { code: 200, data: responseData };
                    } else {
                        responseObj = { code: 500, data: 'xmppRegister error' }
                    }
                } else {
                    responseObj = { code: 400, data: 'Email or password is invalid' };
                }
                return responseObj;
            }).catch(
                (err: any) => {

                    console.log(err);
                    const responseObj = { code: 500, data: { message: 'Internal server error', data: err } };
                    return responseObj;
                },
            );
    }

    async login(body) {

        let response: ResponseObj;
        let params;
        console.log(body);

        switch (body.loginType) {

            case 0:
                params = {
                    pushToken: body.pushToken,
                    password: body.password,
                };
                response = await this.loginDb(params, body.email);
                if (response.code == 400) {
                    response.code = 422;
                    response.data = 'Email or password is invalid';
                }
                break;


            case 1:
                params = {
                    pushToken: body.pushToken,
                    googleId: body.googleId,
                };
                response = await this.loginDb(params, body.email,);
                if (response.code == 400) {
                    response.code = 423;
                    response.data = 'Gmail user  no registered';
                }
                break;

            case 2:
                let email = body.email
                if(email === null) {
                    email = 'Facebook user';
                    console.log('handling facebook user email');
                }
                params = {
                    pushToken: body.pushToken,
                    facebookId: body.facebookId,
                };
                response = await this.loginDb(params, email);
                if (response.code == 400) {
                    response.code = 423;
                    response.data = 'Facebook user  no registered';
                }
                break;
            case 3:
                params = {
                    pushToken: body.pushToken,
                    appleId: body.appleId,
                };
                response = await this.loginDb(params, body.email,);
                if (response.code == 400) {
                    response.code = 423;
                    response.data = 'Apple user  no registered';
                }
                break;
        }

        return response;
    }



}