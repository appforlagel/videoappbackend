import { Injectable } from '@nestjs/common';
import { neode } from 'src/main';
import { ResponseObj } from 'src/utils/interfaces/ResponseObj';

@Injectable()
export class AdminService {

    async create(admin): Promise<any> {
        let responseObj: ResponseObj;
        
        return await neode.merge('Admin', admin).then(
            async (createdRes) => {
                const createdUser = await createdRes.toJson();
                responseObj = { code: 201, data: createdUser };
                return responseObj;
            },
        ).catch(
            (err: any) => {
                console.log(err);
                responseObj = { code: 400, data: err.toString() };
                return responseObj;
            },
        );
    }

    async login(body): Promise<any> {
        return await neode.first('Admin', { email: body.email }, 1).then(
            async (res: any) => {
                console.log(res);
                let responseObj: ResponseObj;
                if (res) {
                    const user = await res.toJson();
                   
                    if (user.password === body.password ) { 
                        //Register success or already exists -> OK
                        const payload = {
                            email: user.email,
                            userId: user.id,
                        };
                        const responseData = {
                            email: user.email,
                            userId: user.id
                        };
                        responseObj = { code: 200, data: responseData };
                    } else {
                        responseObj = { code: 400, data: 'Email or password is invalid' }
                    }
                } else {
                    responseObj = { code: 400, data: 'Email or password is invalid' };
                }
                return responseObj;
            }).catch(
                (err: any) => {

                    console.log(err);
                    const responseObj = { code: 500, data: { message: 'Internal server error', data: err } };
                    return responseObj;
                },
            );
    }

    async update(body,id): Promise<any> {
        return await neode.find('Admin', id).then(
            async (admin: any) => {
                if (admin) {
                    const adminUser = await admin.toJson();
                    if(adminUser.password === body.curentpassword){
                        delete body.curentpassword;
                        return await admin.update(body).then(
                            async (updateRes) => {
                                // const updatedUser = await updateRes.toJson();
                                const responseObj: ResponseObj = { code: 200, data: "Password Successfully changed" };
                                return responseObj;
                            },
                        ).catch(
                            (err) => {
                                console.log(err);
                            }
                        );
                    }else{
                        const responseObj: ResponseObj = { code: 404, data: "Current password didn't matched" };
                        return responseObj;
                    }
                    
                } else {
                    const responseObj: ResponseObj = { code: 404, data: 'The user is not found in the database' };
                    return responseObj;
                }
            },
        );
    }
}
