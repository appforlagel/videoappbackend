import { Injectable } from '@nestjs/common';
import { throws } from 'assert';
import { NodeCollection } from 'neode';
import { neode } from 'src/main';
import { FollowStatusNotification, NotificationType } from 'src/notification/notification.controller';
import { NotificationService } from 'src/notification/notification.service';
import { ResponseObj } from 'src/utils/interfaces/ResponseObj';
import { AuthService } from '../auth.service';
import { createHmac } from 'crypto';
import { AnswerService } from 'src/answer/answer.service';

@Injectable()
export class UserService {

    constructor(private _service: AuthService, private _notificationService: NotificationService,
        private _answerService: AnswerService) { }

    async list(data): Promise<any> {
        let responseObj: ResponseObj;
        return await neode.writeCypher(`
                MATCH (u: User ) return u`, {})
            .then(
                (res) => {
                    console.log(res.records);

                    const result = res.records.map((x) => {
                        const users = x.get('u').properties;
                        return users;
                    });
                    responseObj = { code: 200, data: result };
                    return responseObj;
                }).catch(
                    (err) => {
                        console.log(err);
                        const responseObj: ResponseObj = { code: 400, data: err };
                        return responseObj;
                    }
                );
    }

    async getUser(id): Promise<any> {

        return await neode.find('User', id).then(
            async (user: any) => {
                if (user) {
                    const userJson = await user.toJson();
                    userJson.questionsAsked = await this._getUserQuestionAsked(id);
                    userJson.questionsAnswered = await this._getUserQuestionAnswered(id);
                    userJson.questionsReceived = await this._getUserQuestionsReceived(id);

                    const responseObj: ResponseObj = { code: 200, data: userJson };
                    return responseObj;
                } else {
                    const responseObj: ResponseObj = { code: 404, data: 'The user is not found in the database' };
                    return responseObj;
                }
            },
        );
    }


    async findAll(): Promise<any> {
        let responseObj: ResponseObj;
      return await  neode.readCypher(`
        Match (u: User) return properties(u) as user;
       `, {}).then(
           async res =>{
                responseObj = { code: 201, data: res.records.map((x) => x.get('user')) };
                return responseObj;
           }
       )
    }

    async create(user): Promise<any> {
        // const hasPassword = bcrypt.hashSync(password, 10);
        let responseObj: ResponseObj;
        const userDb = await neode.first('User', { email: user.email }, 1);
        if (!userDb) {
            return await neode.merge('User', user).then(
                async (createdRes) => {
                    const createdUser = await createdRes.toJson();
                    const loginResponse = await this._service.login(user);
                    responseObj = { code: 201, data: loginResponse.data };
                    return responseObj;
                },
            ).catch(
                (err: any) => {
                    console.log(err);
                    responseObj = { code: 400, data: err.toString() };
                    return responseObj;
                },
            );
        } else {
            responseObj = { code: 422, data: 'The user is already registered' };
            return responseObj;
        }


    }

    async update(userInfo, id): Promise<any> {
        return await neode.find('User', id).then(
            async (user: any) => {
                if (user) {
                    return await user.update(userInfo).then(
                        async (updateRes) => {
                           
                            let v1 = user["_properties"];
                            let v = {
                                "id": v1.get("id"),
                                "firstname": v1.get("firstname"),
                                "lastname": v1.get("lastname"),
                                "username": v1.get("username"),
                                "email": v1.get("email"),
                                "googleId": v1.get("googleId"),
                                "facebookId": v1.get("facebookId"),
                                "avatar": v1.get("avatar"),
                                "bio": v1.get("bio"),
                                "job": v1.get("job"),
                                "education": v1.get("education"),
                                "latitude": v1.get("latitude"),
                                "longitude": v1.get("longitude"),
                                "live": v1.get("live"),
                                "privateProfile": v1.get("privateProfile"),
                                "notificationEnable": v1.get("notificationEnable"),
                                "timestamp": v1.get("timestamp"),
                                "questionsReceived": v1.get("questionsReceived"),
                                "questionsAnswered": v1.get("questionsAnswered"),
                                "questionsAsked": v1.get("questionsAsked"),
                                "questionsShared": v1.get("iquestionsSharedd"),
                                "interests": v1.get("interests"),
                                "notifications": v1.get("notifications"),
                                "following": v1.get("following"),
                                "followers": v1.get("followers"),
                                "followNotification": v1.get("followNotification"),
                                "questionForYouNotification": v1.get("questionForYouNotification"),
                                "directMessagesNotification": v1.get("directMessagesNotification"),
                                "likeNotification": v1.get("likeNotification"),
                                "commentNotification": v1.get("commentNotification"),
                                "answerNotification": v1.get("answerNotification"),
                                "interestQuestionNotification": v1.get("interestQuestionNotification"),
                                "blockedUsers": v1.get("blockedUsers"),
                                "interestQuestblockedUsersForionNotification": v1.get("interestQuestblockedUsersForionNotification"),
                            }
                            // const updatedUser = await updateRes.toJson();
                            const responseObj: ResponseObj = { code: 200, data: v };
                            return responseObj;
                        },
                    ).catch(
                        (err) => {
                            console.log(err);
                        }
                    );
                } else {
                    const responseObj: ResponseObj = { code: 404, data: 'The user is not found in the database' };
                    return responseObj;
                }
            },
        );
    }

    async delete(id): Promise<any> {
        return await neode.findById('User', id).then(
            async (user: any) => {
                if (user) {
                    return await user.delete().then(
                        async (deletedRes) => {
                            const deletedUser = await deletedRes.toJson();
                            const responseObj: ResponseObj = { code: 200, data: deletedUser };
                            return responseObj;
                        },
                    );
                } else {
                    const responseObj: ResponseObj = { code: 404, data: 'The user is not found in the database' };
                    return responseObj;
                }
            },
        );
    }

    async __getProfile(id, userRequestId): Promise<any> {
            const isBlocked = await neode.writeCypher(`
                RETURN exists((:User {id:$memberId})-[:BLOCK]->(:User {id:$userId})) as isBlocked
            `, { userId: userRequestId, memberId: id }).then(
                res => {
                    if (res.records.length > 0) {
                        return res.records[0].get('isBlocked');
                    } else {
                        return false;
                    }
                }
            );
            if (isBlocked && userRequestId != null) {
                const responseObj: ResponseObj = { code: 409, data: 'Member user blocked you' };
                return responseObj;
            }
            return await neode.find('User', id).then(
                async (user: any) => {
                    if (user) {
                        const userJson = await user.toJson();
                        userJson.questionsAsked = userJson.questionsAsked.map((x) => {
                            return x.node;
                        })
                        userJson.questionsAnswered = userJson.questionsAnswered.map((x) => {
                            return x.node;
                        })
                        userJson.questionsReceived = userJson.questionsReceived.map((x) => {
                            return x.node;
                        })
                        userJson.notifications = await this._getUserNotifications(user);
                        // userJson.questionsShared = await this._getUserShares(id); this questions are not in use anymore
                        userJson.interests = userJson.interests.map((x) => {
                            return x.node;
                        })
                        userJson.following = userJson.following.map((x) => {
                            x = x.node;
                            return { id: x.id, firstname: x.firstname, lastname: x.lastname, avatar: x.avatar, email: x.email, username: x.username }
                        });
                        userJson.followers = userJson.followers.map((x) => {
                            x = x.node;
                            return { id: x.id, firstname: x.firstname, lastname: x.lastname, avatar: x.avatar, email: x.email, username: x.username }
                        });

                        userJson.blockedUsers = userJson.blockedUsers.map((x) => {
                            x = x.node;
                            return { id: x.id, firstname: x.firstname, lastname: x.lastname, avatar: x.avatar, email: x.email, username: x.username }
                        });

                        userJson.blockedUsersForMessages = userJson.blockedUsersForMessages.map((x) => {
                            x = x.node;
                            return { id: x.id, firstname: x.firstname, lastname: x.lastname, avatar: x.avatar, email: x.email, username: x.username }
                        });

                        //Ejabberd register
                        const key = Buffer.from('my super secret key', 'utf8');
                        userJson.xmppPassword = createHmac('sha512', key).update(userJson.id).digest('base64');
                        const responseObj: ResponseObj = { code: 200, data: userJson };
                        return responseObj;
                    } else {
                        const responseObj: ResponseObj = { code: 404, data: 'The user is not found in the database' };
                        return responseObj;
                    }
                },
            );
        }

    async processUserData(user, relData) {
        const key = Buffer.from('my super secret key', 'utf8');
        const xmppPassword = createHmac('sha512', key).update(user.id).digest('base64');
        let userData = {
            ...user,
            interests: [],
            questionsAsked: [],
            questionsAnswered: [],
            questionsReceived: [],
            following: [],
            followers: [],
            notifications: [],
            likes: [],
            shares: [],
            views: [],
            questionsShare: [],
            blockedUsers: [],
            blockedUsersForMessages: [],
            xmppPassword: xmppPassword
        }
        let idsQuestionAsked = [], idsAnswersGiven = [], 
            idsQuestionReceived = [], idsQuestionShared = [],
            notificationIds =[]
        for (const x of relData.map((x) => x)) {
            switch(x['reltype']) {
                case 'INTERESTED_IN':
                    userData.interests.push(x['data'])
                    break;
                case 'QUESTIONS_ASKED':
                    idsQuestionAsked.push(x['id'])
                    break;
                case 'ANSWER_USER':
                    idsAnswersGiven.push(x['id'])
                    break;
                case 'QUESTIONS_RECEIVED':
                    idsQuestionReceived.push(x['id'])
                    break;
                case 'FOLLOW':
                    if(x['reldirection']) {
                        userData.following.push(x['data'])
                    } else {
                        userData.followers.push(x['data'])
                    }
                    break;
                case 'LIKE':
                    userData.likes.push(x['data'])
                    break;
                case 'SHARE':
                    idsQuestionShared.push(x['id'])
                    break;
                case 'USER_NOTIFICATIONS':
                    notificationIds.push(x['id'])
                    break;
                case 'VIEW':
                    userData.views.push(x['data'])
                    break;
                case 'BLOCK':
                    userData.blockedUsers.push(x['data'])
                    break;
                default:
                    console.log('Unsupported : ' + x['reltype'])
            }
        }
        userData.questionsAsked = await this.getQuestionsAsked(user.id, idsQuestionAsked);
        userData.questionsAnswered = await this.getQuestionsAnswered(user.id, idsAnswersGiven);
        userData.questionsReceived = await this.getQuestionsReceived(user.id, idsQuestionReceived);
        userData.notifications = await this.getUserNotifications(user.id, notificationIds);
        return await userData;
    }

    async getProfile(id, userRequestId): Promise<any> {
        if(id !== userRequestId) {
            const isBlocked = await neode.writeCypher(`
                RETURN exists((:User {id:$memberId})-[:BLOCK]->(:User {id:$userId})) as isBlocked
            `, { userId: userRequestId, memberId: id }).then(
                res => {
                    if (res.records.length > 0) {
                        return res.records[0].get('isBlocked');
                    } else {
                        return false;
                    }
                }
            );
            if (isBlocked && userRequestId != null) {
                const responseObj: ResponseObj = { code: 409, data: 'Member user blocked you' };
                return responseObj;
            }
        }

        return await neode.writeCypher(`
            MATCH (u: User {id: $userId})-[r:INTERESTED_IN|QUESTIONS_ASKED|
                        ANSWER_USER|QUESTIONS_RECEIVED|
                        FOLLOW|LIKE|SHARE|USER_NOTIFICATIONS|VIEW|BLOCK]-(m)
            WITH properties(u) as user, properties(m) as relnode, 
            collect(type(r)) as reltypes, m,u, r
            RETURN user, collect(relnode{id: relnode.id, data: relnode, 
                reltype: type(r), reldirection: (startNode(r) = u)}) as relationships
        `, { userId: id }).then(
            async (userData: any) => {
                if (userData.records.length > 0) {
                    const user = userData.records[0].get('user');
                    const relationships = userData.records[0].get('relationships');
                    const res = await this.processUserData(user, relationships);

                    const responseObj: ResponseObj = { code: 200, data: res };
                    return responseObj;
                } else {
                    const responseObj: ResponseObj = { code: 404, data: 'The user is not found in the database' };
                    return responseObj;
                }
            }
        );
    }

    async getQuestionsAsked(userId, idsQuestionAsked) {
        return await neode.writeCypher(`
        MATCH (u: User {id:$userId})
        MATCH (q: Question)
        WHERE q.id IN $questions
        OPTIONAL MATCH (q)-[an:ANSWER]-> (a:Answer)-[:ANSWER_USER]->(au: User)
        OPTIONAL MATCH (q)<-[sh:SHARE ]-(:User)
        OPTIONAL MATCH (a)<-[like:LIKE]-(:User)
        OPTIONAL MATCH (a)<-[view:VIEW]-(:User)
        OPTIONAL MATCH (q)-[:QUESTIONS_RECEIVED]->(iq: Interest)
        OPTIONAL MATCH (q)-[:QUESTIONS_RECEIVED]->(userReceived: User)
        WITH
        toFloat(count( like)) as likes,
        toFloat(sum(view.views)) as views,
        exists((u)-[:LIKE]->(a)) as userLike,
        exists((u)-[:SHARE]->(q)) as userShare,
        u, q,a, sh, like,iq,au,userReceived
        order by likes desc
        return  q {.*, userShare, shares: toFloat(count(distinct sh)),interest:properties(iq), userAsked: properties(u), userReceived: properties(userReceived)},
            collect(distinct a {.*, userLike , likes, views, userAnswered: properties(au), views}) as answers
            order by q.lastAnswer desc
        `, { userId: userId, questions: idsQuestionAsked }).then(
            async (res) => {
                const finalObj = [];
                for (const x of res.records) {
                    const result = x.get('q');
                    result.answers = x.get('answers');
                    if (result.answers != null && result.answers.length > 0) {
                        const responseObj: ResponseObj = await this._answerService.getCommets(result.answers[0].id);
                        if (responseObj.code == 200) {
                            result.answers[0].comments = responseObj.data;
                        }
                    }
                    finalObj.push(result);
                }
                return finalObj;
            }
        );
    }

    async getQuestionsAnswered(userId, idsAnswersGiven) {
        return await neode.writeCypher(`
            MATCH (u: User {id:$userId}) 
            MATCH (userAsk: User) -[: QUESTIONS_ASKED]->(q: Question)-[an:ANSWER]-> (a: Answer)
            WHERE a.id IN $answers
            OPTIONAL MATCH (q)<-[sh:SHARE ]-(:User) 
            OPTIONAL MATCH (a)<-[like:LIKE]-(:User)
            OPTIONAL MATCH (a)<-[view:VIEW]-(:User)
            OPTIONAL MATCH (q)-[:QUESTIONS_RECEIVED]->(iq: Interest)
            WITH
            toFloat(count( like)) as likes,
            toFloat(sum(view.views)) as views,
            exists((u)-[:LIKE]->(a)) as userLike,
            exists((u)-[:SHARE]->(q)) as userShare,
            u, q,a, sh, like,iq,userAsk
            order by likes desc
            return  q {.*, userShare, shares: toFloat(count(distinct sh)),interest:properties(iq), userAsked: properties(userAsk)},
                collect(distinct a {.*, userLike , likes, views, userAnswered: properties(u), views}) as answers
                order by q.lastAnswer desc
        `, { userId: userId, answers: idsAnswersGiven }).then(
            async (res) => {
                const finalObj = [];
                for (const x of res.records) {
                    const result = x.get('q');
                    result.answers = x.get('answers');
                    if (result && result.answers && result.answers.length > 0) {
                        const responseObj: ResponseObj = await this._answerService.getCommets(result.answers[0].id);
                        if (responseObj.code == 200) {
                            result.answers[0].comments = responseObj.data;
                        }
                    }
                    finalObj.push(result);
                }
                return finalObj;
            }
        );

    }

    async getQuestionsReceived(userId, idsQuestionReceived) {
        return await neode.writeCypher(`
            MATCH (u: User {id:$userId})
            MATCH (q: Question)
            WHERE q.id IN $questions
            MATCH (q)<-[:QUESTIONS_ASKED]-(userAsked: User)
            WHERE not exists((q)-[:ANSWER]->(:Answer))
            return q{.*, userAsked}
        `, { userId: userId, questions: idsQuestionReceived }).then(
            async (res) => {
                return res.records.map((x) => {
                    const result = x.get('q');
                    result.userAsked = result.userAsked.properties;
                    return result;
                })
            }
        );
    }

    async getUserNotifications(userId, notificationIds) {
        return await neode.writeCypher(`
            MATCH (n: Notification)
            WHERE n.id IN $notifications
            OPTIONAL MATCH(n)-[:QUESTION_NOTIFY]->(q:Question)
            OPTIONAL MATCH(n)-[:ANSWER_NOTIFY]->(a:Answer)
            OPTIONAL MATCH(n)-[:RELATE_USER]->(ru:User)
            MATCH (u: User {id: $userId})
            WITH properties(u) as user, n, properties(q) as question, properties(a) as answer, properties(ru) as relatedUser
            RETURN n{.*, user, answer, question, relatedUser}
        `, { userId: userId, notifications: notificationIds }).then(
            async (res) => {
                return res.records.map((x) => {
                    const result = x.get('n');
                    return result;
                })
            }
        );
    }

    async getSimpleProfile(id): Promise<any> {
        return await neode.find('User', id).then(
            async (user: any) => {
                if (user) {
                    const userJson = await user.toJson();
                    //Ejabberd register
                    const key = Buffer.from('my super secret key', 'utf8');
                    userJson.xmppPassword = createHmac('sha512', key).update(userJson.id).digest('base64');
                    const responseObj: ResponseObj = { code: 200, data: userJson };
                    return responseObj;
                } else {
                    const responseObj: ResponseObj = { code: 404, data: 'The user is not found in the database' };
                    return responseObj;
                }
            },
        );
    }

    async geSuggestAndRecentList(id): Promise<any> {
        return await neode.writeCypher(`
            MATCH (u:User {id:$userId})-[:INTERESTED_IN]->(i:Interest)
            MATCH (ou:User)-[:INTERESTED_IN]->(i) Where ID(u)<> ID(ou)
            MATCH (u)-[:INTERESTED_IN]->(ui:Interest)
            return  collect(DISTINCT u) as mainUser,  collect(DISTINCT ou) as otherUsers, collect(DISTINCT ui) as interests
        `, { userId: id }).then(
            async (res) => {
                const responseData = {
                    usersSuggested: res.records[0].get('otherUsers').map((x) => {
                        return x['properties'];
                    }),
                    interests: res.records[0].get('interests').map((x) => { return x['properties'] }),
                    recentUsers: await this.getRecentUsers(id),
                    recentInterests: await this.getRecentInterest(id),
                }
                const responseObj: ResponseObj = { code: 200, data: responseData };
                return responseObj;
            }
        );
    }

    async getSuggestionForFollow(id): Promise<any> {

        return await neode.writeCypher(`
            MATCH (u:User {id:$userId})-[:INTERESTED_IN]->(i:Interest)
            MATCH (ou:User)-[:INTERESTED_IN]->(i) Where ID(u)<> ID(ou) and not exists((u)-[:FOLLOW]->(ou) ) and not exists((ou)-[:BLOCK]->(u) )
            return distinct ou
        `, { userId: id }).then(
            async (res) => {
                const responseData = res.records.map((x) => {
                    return x.get('ou')['properties'];
                });
                const responseObj: ResponseObj = { code: 200, data: responseData };
                return responseObj;
            }
        );

    }

    async getRecentUsers(userId) {
        return await neode.writeCypher(`
            MATCH(u:User {id:$userId})-[:QUESTIONS_ASKED]->
            (q: Question) -[:QUESTIONS_RECEIVED]->(ur:User) 
            return distinct ur.id as id,ur.email as email,  ur.firstname as firstname, ur.lastname as lastname, ur.avatar as avatar,
            collect(q.createdAt)[0] as timestamp Order by timestamp desc
        `, { userId: userId }).then(
            (res) => {
                const foo = res.records.map((x) => {
                    return x.toObject();
                })
                return foo;
            }
        );
    }

    async getRecentInterest(userId) {
        return await neode.writeCypher(`
        MATCH(u:User {id:$userId})-[:QUESTIONS_ASKED]->
        (q: Question) -[:QUESTIONS_RECEIVED]->(i:Interest) 
        return distinct i.id as id, i.label as label,i.icon as icon,  q.createdAt as timestamp Order by q.createdAt desc
    `, { userId: userId }).then(
            (res) => {
                const foo = res.records.map((x) => {
                    return x.toObject();
                })
                return foo;
            }
        );
    }

    async _getUserQuestionAsked(userId) {
        return await neode.writeCypher(`
        MATCH (u: User {id:$userId}) 
        MATCH (u) -[: QUESTIONS_ASKED]->(q: Question)
        OPTIONAL MATCH (q)-[an:ANSWER]-> (a:Answer)-[:ANSWER_USER]->(au: User)
        OPTIONAL MATCH (q)<-[sh:SHARE ]-(:User) 
        OPTIONAL MATCH (a)<-[like:LIKE]-(:User)
        OPTIONAL MATCH (a)<-[view:VIEW]-(:User)
        OPTIONAL MATCH (q)-[:QUESTIONS_RECEIVED]->(iq: Interest)
        OPTIONAL MATCH (q)-[:QUESTIONS_RECEIVED]->(userReceived: User)
        WITH 
        toFloat(count( like)) as likes , 
        toFloat(sum(view.views)) as views,
        exists((u)-[:LIKE]->(a)) as userLike, 
        exists((u)-[:SHARE]->(q)) as userShare,
        u, q,a, sh, like,iq,au,userReceived
        order by likes desc
        return  q {.*, userShare, shares: toFloat(count(distinct sh)),interest:properties(iq), userAsked: properties(u), userReceived: properties(userReceived)},
            collect(distinct a {.*, userLike , likes, views, userAnswered: properties(au), views}) as answers 
            order by q.lastAnswer desc 
            
        `, { userId: userId }).then(
            async (res) => {
                const finalObj = [];
                for (const x of res.records) {
                    const result = x.get('q');
                    result.answers = x.get('answers');
                    if (result.answers != null && result.answers.length > 0) {
                        const responseObj: ResponseObj = await this._answerService.getCommets(result.answers[0].id);
                        if (responseObj.code == 200) {
                            result.answers[0].comments = responseObj.data;
                        }
                    }
                    finalObj.push(result);
                }
                return finalObj;
            }
        );

    }

    async _getUserQuestionAnswered(userId) {
        return await neode.writeCypher(`
            MATCH (userAsk: User) -[: QUESTIONS_ASKED]->(q: Question)-[an:ANSWER]-> (a:Answer)-[:ANSWER_USER]->(u: User {id:$userId})
            OPTIONAL MATCH (q)<-[sh:SHARE ]-(:User) 
            OPTIONAL MATCH (a)<-[like:LIKE]-(:User)
            OPTIONAL MATCH (a)<-[view:VIEW]-(:User)
            OPTIONAL MATCH (q)-[:QUESTIONS_RECEIVED]->(iq: Interest)
            WITH 
            toFloat(count( like)) as likes , 
            toFloat(sum(view.views)) as views,
            exists((u)-[:LIKE]->(a)) as userLike, 
            exists((u)-[:SHARE]->(q)) as userShare,
            u, q,a, sh, like,iq,userAsk
            order by likes desc
            return  q {.*, userShare, shares: toFloat(count(distinct sh)),interest:properties(iq), userAsked: properties(userAsk)},
                collect(distinct a {.*, userLike , likes, views, userAnswered: properties(u), views}) as answers 
                order by q.lastAnswer desc 
        `, { userId: userId }).then(
            async (res) => {
                const finalObj = [];
                for (const x of res.records) {
                    const result = x.get('q');
                    result.answers = x.get('answers');
                    if (result && result.answers && result.answers.length > 0) {
                        const responseObj: ResponseObj = await this._answerService.getCommets(result.answers[0].id);
                        if (responseObj.code == 200) {
                            result.answers[0].comments = responseObj.data;
                        }
                    }
                    finalObj.push(result);
                }
                return finalObj;
            }
        );

    }

    async _getUserQuestionsReceived(userId) {
        return await neode.writeCypher(`
            MATCH (u: User {id:$userId})  <-[:QUESTIONS_RECEIVED]-(q: Question)<-[:QUESTIONS_ASKED]-(userAsked: User) WHERE 
            not exists((q)-[:ANSWER]->(:Answer)) 
            return q{.*, userAsked}
        `, { userId: userId }).then(
            async (res) => {
                return res.records.map((x) => {
                    const result = x.get('q');
                    result.userAsked = result.userAsked.properties;
                    return result;
                })
            }
        );
    }

    async _getUserNotifications(user) {
        const result = [];
        const notifications: NodeCollection = user.get('notifications');

        for (const x of notifications.map((x) => x)) {
            const notification = (await x.toJson()).node;
            notification.user = notification.user?.node;
            notification.question = notification.question?.node;
            notification.relatedUser = notification.relatedUser ? notification.relatedUser.node : notification.question==null?null:notification.question.userAsked;
            result.push(notification);
        }
        result.sort((a, b) => {
            const date1 = new Date(a.date);
            const date2 = new Date(b.date);
            if (date1 > date2) {
                return 1;
            } else if (date1 < date2) {
                return -1;
            } else {
                return 0;
            }

        })
        return result;
    }

    async _getUserShares(userId) {
        return await neode.writeCypher(`
        MATCH (u {id: $userId})-[:SHARE] ->(q: Question)<-[:QUESTIONS_ASKED]-(userAsked: User)
        OPTIONAL MATCH (q)-[:QUESTIONS_RECEIVED]->(userReceived: User)
        OPTIONAL MATCH (q)-[:QUESTIONS_RECEIVED]->(interest: Interest)
        OPTIONAL MATCH (q)-[:ANSWER] ->(a: Answer)-[:ANSWER_USER]->(userAnswered: User)
        OPTIONAL MATCH (q)<-[sh:SHARE ]-(:User) 
        OPTIONAL MATCH (a)<-[up:UPVOTE]-(:User)
        OPTIONAL MATCH (a)<-[do:DOWNVOTE ]-(:User)
        OPTIONAL MATCH (a)<-[view:VIEW]-(:User)
        with 
        toFloat(count(sh)) as shares,
        toFloat(count(up)) as upVotes , 
        toFloat(count(do)) as downVotes, 
        toFloat(sum(view.views)) as views,
        a{.*, userAnswered} as answer, q,userReceived,userAsked, interest,  exists((u)-[:UPVOTE]->(a)) as userUpVote, exists((u)-[:DOWNVOTE]->(a)) as userDownVote,
        exists((u)-[:SHARE]->(q)) as userShare
        return  answer {.*,userUpVote, userDownVote, upVotes,downVotes, views}, q {.*, userUpVote,userAsked, userShare, userDownVote, shares, upVotes,downVotes, answer, userReceived, interest}
        ORDER BY q.createdAt desc
    `, { userId: userId }).then(
            async (res) => {
                const finalObj = [];
                res.records.forEach(async (x) => {
                    const result = x.get('q');
                    result.answer = x.get('answer');
                    if (result && result.answer) {
                        const responseObj: ResponseObj = await this._answerService.getCommets(result.answer.id);
                        if (responseObj.code == 200) {
                            result.answer.comments = responseObj.data;
                        }
                        result.answer.userAnswered = result.answer.userAnswered.properties;
                    }
                    if (result.userAsked) {
                        result.userAsked = result.userAsked.properties;
                    }
                    if (result.userReceived) {
                        result.userReceived = result.userReceived.properties;
                    }
                    if (result.interest) {
                        result.interest = result.interest.properties;
                    }

                    finalObj.push(result);
                })
                return finalObj;
            }
        );
    }

    async getUsersByQuery(query) {
        let responseObj: ResponseObj;
        return await neode.writeCypher(`
        MATCH (u:User) WHERE toLower(u.firstname) CONTAINS $query
        or toLower(u.lastname) CONTAINS $query or toLower(split(u.email, '@')[0]) CONTAINS $query return u
    `, { query: query }).then(
            async (res) => {
                const result = res.records.map((record) => {
                    const simpleUser = record.get('u').properties;
                    return { avatar: simpleUser.avatar, firstname: simpleUser.firstname, lastname: simpleUser.lastname, email: simpleUser.email, id: simpleUser.id };
                });

                responseObj = { code: 200, data: result };
                return responseObj;
            }
        );
    }

    async follow(body) {
        let responseObj: ResponseObj;
        try {
            const user = await neode.find('User', body.userId);
            const member = await neode.find('User', body.memberId);

            if (user && member) {
                const relationExist = await neode.writeCypher(`
                    MATCH (u:User {id:$userId})-[f:FOLLOW]->(m:User {id:$memberId})
                 return  f
                `, { userId: body.userId, memberId: body.memberId });
                if (relationExist.records.length <= 0) {
                    if (!await this.checkUserBlockAndRemove(body.userId, body.memberId)) {
                        this._notificationService.createFollowNotification(body.userId, body.memberId);
                        if (!member.get('privateProfile')) {
                            await user.relateTo(member, 'following');
                            responseObj = { code: 200, data: member.properties() };
                        } else {
                            responseObj = { code: 209, data: 'Wait for answer' };
                        }
                    } else {
                        responseObj = { code: 210, data: 'The other user has blocked you' };
                    }

                } else {
                    responseObj = { code: 400, data: 'user already followed' };
                }


                return responseObj
            } else {
                responseObj = { code: 400, data: 'User or member not found' };
                return responseObj
            }

        } catch (e) {
            console.log(e);
            responseObj = { code: 500, data: e };
            return responseObj
        }
    }

    async unfollow(body) {
        let responseObj: ResponseObj;
        try {
            return await neode.writeCypher(`
             MATCH (u:User {id:$userId})-[f:FOLLOW]->(m:User {id:$memberId})
             optional Match (u: User {id:$memberId})-[ut:USER_NOTIFICATIONS]->(nt: Notification {type: 'follow'})-[ru:RELATE_USER]-> (ou:User {id:$userId}) 
                detach delete  f, nt
            `, { userId: body.userId, memberId: body.memberId }).then((res) => {
                responseObj = { code: 200, data: 'Success' };
                return responseObj
            }).catch((err) => {
                responseObj = { code: 400, data: err };
                return responseObj
            });
        } catch (e) {
            console.log(e);
            responseObj = { code: 500, data: e };
            return responseObj
        }
    }

    async acceptFollow(body) {
        let responseObj: ResponseObj;
        try {
            const user = await neode.find('User', body.userId);
            const member = await neode.find('User', body.memberId);

            if (user && member) {
                await member.relateTo(user, 'following');
                await neode.writeCypher(`
                    Match (u: User {id:$userId})-[ut:USER_NOTIFICATIONS]->(nt: Notification {type: 'follow'})
                    -[ru:RELATE_USER]-> (ou:User {id:$memberId}) set nt.followStatus = $followStatus
                `, { userId: body.userId, memberId: body.memberId, followStatus: FollowStatusNotification.ACCEPTED });

                responseObj = { code: 200, data: 'Success' };
                return responseObj
            } else {
                responseObj = { code: 400, data: 'User or member not found' };
                return responseObj
            }

        } catch (e) {
            console.log(e);
            responseObj = { code: 500, data: e };
            return responseObj
        }
    }

    async declineFollow(body) {
        let responseObj: ResponseObj;
        try {
            const user = await neode.find('User', body.userId);
            const member = await neode.find('User', body.memberId);

            if (user && member) {
                await neode.writeCypher(`
                    Match (u: User {id:$userId})-[ut:USER_NOTIFICATIONS]->(nt: Notification {type: 'follow'})
                    -[ru:RELATE_USER]-> (ou:User {id:$memberId}) set nt.followStatus = $followStatus
                `, { userId: body.userId, memberId: body.memberId, followStatus: FollowStatusNotification.DECLINED });

                responseObj = { code: 200, data: 'User declined' };
                return responseObj
            } else {
                responseObj = { code: 400, data: 'User or member not found' };
                return responseObj
            }

        } catch (e) {
            console.log(e);
            responseObj = { code: 500, data: e };
            return responseObj
        }
    }

    async blockUser(body) {
        let responseObj: ResponseObj;
        try {
            await neode.writeCypher(`
                MATCH (u:User {id:$userId}), (m:User {id:$memberId})
                OPTIONAL MATCH (u)-[uf:FOLLOW]->(m)
                OPTIONAL MATCH (m)-[mf:FOLLOW]->(u)
                MERGE (u)-[b:BLOCK]->(m)
                DETACH DELETE uf,mf                    
                RETURN  b
            `, { userId: body.userId, memberId: body.memberId });

            responseObj = { code: 200, data: 'Success' };
            return responseObj;
        } catch (e) {
            console.log(e);
            responseObj = { code: 500, data: e };
            return responseObj
        }
    }

    async unBlockUser(body) {
        let responseObj: ResponseObj;
        try {
            await neode.writeCypher(`
                MATCH (u:User {id:$userId})-[b:BLOCK]->(m:User {id:$memberId})                  
                detach delete b
            `, { userId: body.userId, memberId: body.memberId });
            responseObj = { code: 200, data: 'Success' };
            return responseObj

        } catch (e) {
            console.log(e);
            responseObj = { code: 500, data: e };
            return responseObj
        }
    }

    async blockMessageUser(body) {
        let responseObj: ResponseObj;
        try {
            await neode.writeCypher(`
                MATCH (u:User {id:$userId}), (m:User {id:$memberId})
                MERGE (u)-[b:MESSAGE_BLOCK]->(m)                    
                return  b
            `, { userId: body.userId, memberId: body.memberId });

            responseObj = { code: 200, data: 'Success' };
            return responseObj;
        } catch (e) {
            console.log(e);
            responseObj = { code: 500, data: e };
            return responseObj
        }
    }

    async unBlockMessageUser(body) {
        let responseObj: ResponseObj;
        try {
            await neode.writeCypher(`
                MATCH (u:User {id:$userId})-[b:MESSAGE_BLOCK]->(m:User {id:$memberId})                  
                detach delete b
            `, { userId: body.userId, memberId: body.memberId });
            responseObj = { code: 200, data: 'Success' };
            return responseObj

        } catch (e) {
            console.log(e);
            responseObj = { code: 500, data: e };
            return responseObj
        }
    }

    async checkUserBlockAndRemove(userId, memberId) {
        return await neode.writeCypher(`
            MATCH (u:User {id:$userId}),(ub:User {id:$memberId}) 
            OPTIONAL MATCH (u)-[block:BLOCK]->(ub)
            DETACH DELETE block
            RETURN exists((ub)-[:BLOCK]->(u)) as iBlocked
        `, { userId: userId, memberId: memberId }).then(
            (res) => {
                return res.records[0].get('iBlocked');
            }
        );
    }
}
