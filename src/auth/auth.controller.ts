import { Controller, Post, Body, Res } from '@nestjs/common';
import { AuthService } from './auth.service';
import { ResponseObj } from 'src/utils/interfaces/ResponseObj';

@Controller('auth')
export class AuthController {

    constructor(private service: AuthService) { }

    @Post('/login')
    async login(@Body() body, @Res() res): Promise<any> {
        const responseObj: ResponseObj = await this.service.login(body);
        res.status(responseObj.code).json(responseObj.data);
    }
}
