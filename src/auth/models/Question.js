
module.exports = {
    id: {
        primary: true,
        type: 'uuid',
        required: true,
    },
    
    text: {
        type: 'string',
        required: true,
    },
    createdAt: {
        type: "number",
        default: new Date().getTime(),
        required: true
    },
    lastAnswer: {
        type: "number",
    },
    privacy: {
        type: 'string',
        default: 'Public'
    },
    saveForLater: {
        type: 'boolean',
        default: false
    },
    archived: {
        type: 'boolean',
        default: false
    },
    appQuestion: {
        type: 'boolean',
        default: false
    },
    userAsked: {
        type: "relationships",
        target: "User",
        relationship: "QUESTIONS_ASKED",
        direction: "in",
        eager: true,
    },
    userReceived: {
        type: "relationship",
        target: "User",
        relationship: "QUESTIONS_RECEIVED",
        direction: "out",
        eager: true,
    },

    interestReceived: {
        type: "relationship",
        target: "User",
        relationship: "QUESTIONS_RECEIVED",
        direction: "out",
    },
    answers: {
        type: "relationships",
        target: "Answer",
        relationship: "ANSWER",
        direction: "out",
        eager: true
    },

    notifications: {
        type: 'relationships',
        target: "Notification",
        relationship: "QUESTION_NOTIFY",
        direction: "in",
        eager: true,
    },
}