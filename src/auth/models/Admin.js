
module.exports = {
    id: {
        primary: true,
        type: 'uuid',
        required: true,
    },
    email: {
        type: 'string',
        unique: true,
        required: true,
    },
    password: {
        type: "string",
        required: true,
    }
};
