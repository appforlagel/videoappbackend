module.exports = {
    id: {
        primary: true,
        type: 'uuid',
        required: true,
    },
    type: {
        type: 'string',
        valid: ['follow', 'like', 'interestAsk', 'directQuestion', 'comment', 'app', 'answer']
    },
    body: {
        type: 'string',
    },
    //Only for follow notification type
    followStatus: {
        type: 'string',
        valid: ['pending', 'accepted', 'declined']
    },
    createdAt: {
        type: "number",
        default: () => new Date().getTime(),
        required: true
    },
    //Only for like notification
    likeCount: {
        type: "number",
        default: 0,
    },
    read: {
        type: 'boolean',
        default: () => false
    },
    user: {
        type: "relationship",
        target: "User",
        relationship: "USER_NOTIFICATIONS",
        direction: "in",
        eager: true,
    },
    question: {
        type: 'relationship',
        target: "Question",
        relationship: "QUESTION_NOTIFY",
        direction: "out",
        eager: true,
    },
    answer: {
        type: 'relationship',
        target: "Answer",
        relationship: "ANSWER_NOTIFY",
        direction: "out",
        eager: true,
    },
    relatedUser: {
        type: "relationship",
        target: "User",
        relationship: "RELATE_USER",
        direction: "out",
        eager: true,
    },


}