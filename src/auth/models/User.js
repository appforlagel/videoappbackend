
module.exports = {
    id: {
        primary: true,
        type: 'uuid',
        required: true,
    },
    firstname: {
        type: 'string',
    },
    lastname: {
        type: 'string',
    },
    username: {
        type: 'string',
        unique: true
    },
    avatar: {
        type: 'string',
        allow: '',
    },
    email: {
        type: 'string',
        unique: true,
        required: true,
    },
    password: {
        type: "string",
        required: true,
    },
    googleId: {
        type: "string",
        unique: true
    },
    facebookId: {
        type: 'string',
        unique: true
    },
    appleId: {
        type: 'string',
        unique: true
    },
    bio: {
        type: 'string'
    },
    job: {
        type: 'string',
    },
    education: {
        type: 'string'
    },
    latitude: {
        type: 'float'
    },
    logitude: {
        type: 'float'
    },
    address: {
        type: 'string'
    },
    live: {
        type: 'string'
    },
    privateProfile: {
        type: 'boolean',
        default: false
    },
    pushToken: {
        type: 'string'
    },
    notificationEnable: {
        type: 'boolean',
        default: true
    },
    followNotification: {
        type: 'boolean',
        default: true
    },
    questionForYouNotification: {
        type: 'boolean',
        default: true
    },
    directMessagesNotification: {
        type: 'boolean',
        default: true
    },
    likeNotification: {
        type: 'boolean',
        default: true
    },
    commentNotification: {
        type: 'boolean',
        default: true
    },
    answerNotification: {
        type: 'boolean',
        default: true
    },
    interestQuestionNotification: {
        type: 'boolean',
        default: true
    },

    interests: {
        type: "relationships",
        target: "Interests",
        relationship: "INTERESTED_IN",
        direction: "out",
        eager: true // <-- eager load this relationship
    },

    questionsAsked: {
        type: "relationships",
        target: "Questions",
        relationship: "QUESTIONS_ASKED",
        direction: "out",
        eager: true
    },
    questionsAnswered: {
        type: "relationships",
        target: "User",
        relationship: "ANSWER_USER",
        direction: "out",
        eager: true
    },
    questionsReceived: {
        type: "relationships",
        target: "Questions",
        relationship: "QUESTIONS_RECEIVED",
        direction: "in",
        eager: true
    },
    following: {
        type: "relationships",
        target: "User",
        relationship: "FOLLOW",
        direction: "out",
        eager: true
    },
    followers: {
        type: "relationships",
        target: "User",
        relationship: "FOLLOW",
        direction: "in",
        eager: true
    },

    answersLike: {
        type: "relationships",
        target: "Answer",
        relationship: "LIKE",
        direction: "out",
    },

    questionsShare: {
        type: "relationships",
        target: "Question",
        relationship: "SHARE",
        direction: "out",
        eager: true
    },

    notifications: {
        type: "relationships",
        target: "Notification",
        relationship: "USER_NOTIFICATIONS",
        direction: "out",
        eager: true
    },

    viewedVideo: {
        type: "relationships",
        target: "Answers",
        relationship: "VIEW",
        direction: "out",
        properties: {
            views: "integer"
        }
    },

    blockedUsers: {
        type: "relationships",
        target: "User",
        relationship: "BLOCK",
        direction: "out",
        eager: true
    },

    blockedUsersForMessages: {
        type: "relationships",
        target: "User",
        relationship: "MESSAGE_BLOCK",
        direction: "out",
        eager: true
    }


};
