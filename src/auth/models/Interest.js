module.exports = {
    id: {
        primary: true,
        type: 'uuid',
        required: true,
    },
    label: {
        type: "string",

    },
    icon: {
        type: "string"
    },
    users: {
        type: "relationship",
        target: "User",
        relationship: "INTERESTED_IN",
        direction: "in",
    }
}