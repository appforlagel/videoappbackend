
module.exports = {
    id: {
        primary: true,
        type: 'uuid',
        required: true,
    },

    video: {
        type: 'string',
        required: true,
    },

    cloudinaryPublicId: {
        type: 'string',
        required: true,
    },

    createdAt: {
        type: "number",
        default: Date.now(),
        required: true
    },
    answerUser: {
        type: "relationship",
        target: "User",
        relationship: "ANSWER_USER",
        direction: "out",
        eager: true
    },
    notifications: {
        type: 'relationships',
        target: "Notification",
        relationship: "ANSWER_NOTIFY",
        direction: "in",
        eager: true,
    },

}