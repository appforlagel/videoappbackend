import { Controller, Body, Post, Res, Put, Param, Delete, Get, Req, UseGuards } from '@nestjs/common';
import { ResponseObj } from 'src/utils/interfaces/ResponseObj';
import { UserService } from '../services/user.service';
import { Response } from 'express'
import { AuthGuard } from '@nestjs/passport';
import { AuthUser } from '../user.decorator';
// import { NotificationService } from 'src/notification/notification.service';
@Controller('user')
export class UserController {
    constructor(private userService: UserService) { }

    @Post('list')
    async list(@Body() data, @Res() res: Response): Promise<any> {
        console.log(data);
        const responseObj: ResponseObj = await this.userService.list(data);
        //console.log(responseObj);
        res.status(responseObj.code).json(responseObj.data);
    }
    @Get('details/:id')
    async getUserDetails(@Param('id') id: string, @Res() res: Response): Promise<any> {
        console.log(id);
        const responseObj: ResponseObj = await this.userService.getUser(id);
        //console.log(responseObj);
        res.status(responseObj.code).json(responseObj.data);
    }

    @Get()
    async getAllUsers(@Res() res: Response): Promise<any>{
        const responseObj: ResponseObj = await this.userService.findAll();
        res.status(responseObj.code).json(responseObj.data);
    }

    @Post()
    async create(@Body() user, @Res() res: Response): Promise<any> {
        const responseObj: ResponseObj = await this.userService.create(user);
        res.status(responseObj.code).json(responseObj.data);
    }

    @Put(':id')
    async update(@Body() user, @Res() res: Response, @Param() params): Promise<any> {
        const responseObj: ResponseObj = await this.userService.update(user, params.id);
        res.status(responseObj.code).json(responseObj.data);
    }

    @Delete(':id')
    async remove(@Param('id') id: string, @Res() res: Response): Promise<any> {
        const responseObj: ResponseObj = await this.userService.delete(id);
        res.status(responseObj.code).json(responseObj.data);
    }

    @Get('getProfile/:id')
    @UseGuards(AuthGuard('jwt'))
    async getProfile(@Param('id') id: string, @Res() res: Response, @AuthUser() user: any): Promise<any> {
        const responseObj: ResponseObj = await this.userService.getProfile(id, user.userId);
        res.status(responseObj.code).json(responseObj.data);
    }

    @Get('getSuggestAndRecentList/:id')
    async getSuggestandRecentListOfUser(@Param('id') id: string, @Res() res: Response): Promise<any> {
        const responseObj: ResponseObj = await this.userService.geSuggestAndRecentList(id);
        res.status(responseObj.code).json(responseObj.data);
    }

    @Post('getUsersByQuery')
    async getUsersByQuery(@Body() body, @Res() res: Response): Promise<any> {
        const responseObj: ResponseObj = await this.userService.getUsersByQuery(body.query);
        res.status(responseObj.code).json(responseObj.data);
    }

    @Get('getSuggestionForFollow/:id')
    async getSuggestionForFollow(@Param('id') id: string, @Res() res: Response): Promise<any> {
        const responseObj: ResponseObj = await this.userService.getSuggestionForFollow(id);
        res.status(responseObj.code).json(responseObj.data);
    }

    @Post('follow')
    async followUser(@Body() body, @Res() res: Response): Promise<any> {
        const responseObj: ResponseObj = await this.userService.follow(body);
        res.status(responseObj.code).json(responseObj.data);
    }
    @Post('unfollow')
    async unfollowUser(@Body() body, @Res() res: Response): Promise<any> {
        const responseObj: ResponseObj = await this.userService.unfollow(body);
        res.status(responseObj.code).json(responseObj.data);
    }

    @Post('acceptFollow')
    async acceptFollow(@Body() body, @Res() res: Response): Promise<any> {
        const responseObj: ResponseObj = await this.userService.acceptFollow(body);
        res.status(responseObj.code).json(responseObj.data);
    }

    @Post('declineFollow')
    async declineFollow(@Body() body, @Res() res: Response): Promise<any> {
        const responseObj: ResponseObj = await this.userService.declineFollow(body);
        res.status(responseObj.code).json(responseObj.data);
    }


    @Post('blockUser')
    async blockUser(@Body() body, @Res() res: Response): Promise<any> {
        const responseObj: ResponseObj = await this.userService.blockUser(body);
        res.status(responseObj.code).json(responseObj.data);
    }

    @Post('unblockUser')
    async unblockUser(@Body() body, @Res() res: Response): Promise<any> {
        const responseObj: ResponseObj = await this.userService.unBlockUser(body);
        res.status(responseObj.code).json(responseObj.data);
    }


    @Post('blockUserMessage')
    async blockUserMessage(@Body() body, @Res() res: Response): Promise<any> {
        const responseObj: ResponseObj = await this.userService.blockMessageUser(body);
        res.status(responseObj.code).json(responseObj.data);
    }

    @Post('unblockUserMessage')
    async unblockUserMessage(@Body() body, @Res() res: Response): Promise<any> {
        const responseObj: ResponseObj = await this.userService.unBlockMessageUser(body);
        res.status(responseObj.code).json(responseObj.data);
    }


    // @Post('test')
    // async onlyForTest(@Body() body, @Res() res: Response): Promise<any> {
    //     // const responseObj: ResponseObj = await this.notificationService.createFollowNotification(body.userId, body.userToFollowId);
    //     // res.status(responseObj.code).json(responseObj.data);
    // }

    // @Get('getUserQuestions/:id')
    // async getUserQuestions(@Param('id') id: string, @Res() res: Response): Promise<any> {
    //     const responseObj: ResponseObj = await this.userService.getUserQuestions(id);
    //     res.status(responseObj.code).json(responseObj.data);
    // }
}
