import { Controller, Body, Post, Res, Put, Param } from '@nestjs/common';
import { ResponseObj } from 'src/utils/interfaces/ResponseObj';
import { Response } from 'express'
import { AdminService } from '../services/admin.service';

@Controller('admin')
export class AdminController {
    constructor(private adminService: AdminService) { }

    @Post()
    async create(@Body() admin, @Res() res: Response): Promise<any> {
        const responseObj: ResponseObj = await this.adminService.create(admin);
        res.status(responseObj.code).json(responseObj.data);
    }

    @Post('login')
    async login(@Body() admin, @Res() res: Response): Promise<any> {
        const responseObj: ResponseObj = await this.adminService.login(admin);
        res.status(responseObj.code).json(responseObj);
    }

    @Put(':id')
    async update(@Body() admin, @Res() res: Response, @Param() params): Promise<any> {
        const responseObj: ResponseObj = await this.adminService.update(admin,params.id);
        res.status(responseObj.code).json(responseObj);
    }
}
