import { Module, HttpModule } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from './constants';
import { JwtStrategy } from './jwt.strategy';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { UserController } from './controllers/user.controller';
import { UserService } from './services/user.service';
import { EjabberdController } from '../chat/controllers/ejabberd.controller'
import { TypeOrmModule } from '@nestjs/typeorm';
import { Answer } from 'src/models/postgresEntities/AnswerEntity';
import { Question } from 'src/models/postgresEntities/QuestionEntity';
import { User } from 'src/models/postgresEntities/UserEntity';
import { VoteToComment } from 'src/models/postgresEntities/VoteToCommentEntity';
import { Comment } from 'src/models/postgresEntities/CommentEntity';
import { AnswerService } from 'src/answer/answer.service';
import { AdminController } from './controllers/admin.controller';
import { AdminService } from './services/admin.service';
import { Admin } from 'src/models/postgresEntities/AdminEntity';

@Module({
    imports: [
        TypeOrmModule.forFeature([User, Comment, Answer, Question, VoteToComment,Admin]),
        PassportModule,
        HttpModule,
        JwtModule.register({
            secret: jwtConstants.secret,
            signOptions: { expiresIn: '1y' },
        }),
    ],
    providers: [AuthService, JwtStrategy, UserService, EjabberdController, AnswerService,AdminService],
    exports: [AuthService,AdminService],
    controllers: [AuthController, UserController,AdminController],
})
export class AuthModule { }
