"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const nestjs_fcm_1 = require("nestjs-fcm");
const main_1 = require("../main");
const CommentEntity_1 = require("../models/postgresEntities/CommentEntity");
const ResponseObj_1 = require("../utils/interfaces/ResponseObj");
const notification_controller_1 = require("./notification.controller");
let NotificationService = class NotificationService {
    constructor(fcmService) {
        this.fcmService = fcmService;
    }
    async getUserNotifications(userId) {
        var _a, _b;
        const result = [];
        const user = await main_1.neode.first('User', { id: userId }, 1);
        const notifications = user.get('notifications');
        for (const x of notifications.map((x) => x)) {
            const notification = (await x.toJson()).node;
            notification.user = (_a = notification.user) === null || _a === void 0 ? void 0 : _a.node;
            notification.question = (_b = notification.question) === null || _b === void 0 ? void 0 : _b.node;
            notification.relatedUser = notification.relatedUser ? notification.relatedUser.node : notification.question.userAsked;
            result.push(notification);
        }
        result.sort((a, b) => {
            const date1 = new Date(a.date);
            const date2 = new Date(b.date);
            if (date1 > date2) {
                return 1;
            }
            else if (date1 < date2) {
                return -1;
            }
            else {
                return 0;
            }
        });
        return result;
    }
    async create(notification) {
        let responseObj;
        return await main_1.neode.merge('Notification', notification).then(async (res) => {
            const createdNotification = await res.toJson();
            responseObj = { code: 201, data: createdNotification };
            return responseObj;
        }).catch((err) => {
            responseObj = { code: 500, data: err.toString() };
            return responseObj;
        });
    }
    async createCommentNotification(userCommentedId, answerId, comment) {
        let responseObj;
        const userCommented = await main_1.neode.first('User', { id: userCommentedId }, 1);
        const answer = await main_1.neode.first('Answer', { id: answerId }, 1);
        const pushNotificationTitle = userCommented.get('username') != null ? userCommented.get('username') : userCommented.get('firstname') != null ? userCommented.get('firstname') : 'Some user';
        if (userCommented && answer) {
            const collection = answer.get('answerUser');
            const collectionJson = await collection.toJson();
            const userToNotify = await main_1.neode.first('User', { id: collectionJson['node'].id }, 1);
            if (userToNotify.get('id') != userCommentedId && !await this.existsNotificationTypeInAnswer(answer, notification_controller_1.NotificationType.COMMENT, userCommented)) {
                const notification = await main_1.neode.merge('Notification', { type: notification_controller_1.NotificationType.COMMENT });
                await notification.relateTo(userToNotify, 'user');
                await notification.relateTo(answer, 'answer');
                await notification.relateTo(userCommented, 'relatedUser');
                if (userToNotify.get('notificationEnable') && userToNotify.get('commentNotification')) {
                    this.sendFcmNotification([userToNotify.get('pushToken')], pushNotificationTitle, comment.body, '');
                }
                responseObj = { code: 200, data: await notification.toJson() };
            }
            else {
                responseObj = { code: 200, data: 'no notification required' };
            }
            return responseObj;
        }
        else {
            responseObj = { code: 422, data: 'The user or question not found' };
            return responseObj;
        }
    }
    async createAnswerNotification(userId, questionId) {
        let responseObj;
        const user = await main_1.neode.first('User', { id: userId }, 1);
        const question = await main_1.neode.first('Question', { id: questionId }, 1);
        const pushNotificationTitle = user.get('username') != null ? user.get('username') : user.get('firstname') != null ? user.get('firstname') : 'Some user';
        if (user && question) {
            const collection = question.get('userAsked');
            const userToNotify = await main_1.neode.first('User', { id: (await collection.toJson())[0].node.id }, 1);
            if (userToNotify.get('id') != userId && !await this.existsNotificationTypeInQuestion(question, notification_controller_1.NotificationType.ANSWER)) {
                const notification = await main_1.neode.merge('Notification', { type: notification_controller_1.NotificationType.ANSWER });
                await notification.relateTo(userToNotify, 'user');
                await notification.relateTo(question, 'question');
                await notification.relateTo(user, 'relatedUser');
                if (userToNotify.get('notificationEnable') && userToNotify.get('answerNotification')) {
                    this.sendFcmNotification([userToNotify.get('pushToken')], pushNotificationTitle, 'Has answer your question', '');
                }
                responseObj = { code: 200, data: await notification.toJson() };
            }
            else {
                responseObj = { code: 200, data: 'no notification required' };
            }
            return responseObj;
        }
        else {
            responseObj = { code: 422, data: 'The user or question not found' };
            return responseObj;
        }
    }
    async createLikeNotification(userId, answerId) {
        let responseObj;
        const user = await main_1.neode.first('User', { id: userId }, 1);
        const answer = await main_1.neode.first('Answer', { id: answerId }, 1);
        const pushNotificationTitle = user.get('username') != null ? user.get('username') : user.get('firstname') != null ? user.get('firstname') : 'Some user';
        if (user && answer) {
            const relationsip = answer.get('answerUser');
            const userToNotify = relationsip.endNode();
            if (userToNotify.get('id') != userId) {
                if (!await this.existLikeNotificationOnAnswer(answerId)) {
                    const notification = await main_1.neode.merge('Notification', { type: notification_controller_1.NotificationType.LIKE, likeCount: 1 });
                    await notification.relateTo(userToNotify, 'user');
                    await notification.relateTo(answer, 'answer');
                    await notification.relateTo(user, 'relatedUser');
                    responseObj = { code: 200, data: await notification.toJson() };
                }
                else {
                    this.updateLikeRelationshipOnAnswer(answerId, user.get('id'));
                    responseObj = { code: 200, data: "notification updated" };
                }
                if (userToNotify.get('notificationEnable') && userToNotify.get('likeNotification')) {
                    this.sendFcmNotification([userToNotify.get('pushToken')], pushNotificationTitle, 'Liked your post ', '');
                }
            }
            else {
                responseObj = { code: 200, data: 'no notification required' };
            }
            return responseObj;
        }
        else {
            responseObj = { code: 422, data: 'The user or question not found' };
            return responseObj;
        }
    }
    async updateLikeRelationshipOnAnswer(answerId, userId) {
        main_1.neode.writeCypher(`
            MATCH (a:Answer {id:$answerId}), (u: User {id:$userId})
            MATCH (a) <-[:ANSWER_NOTIFY]-(n:Notification {type:"like"})-[rur:RELATE_USER]->(ru: User) 
            OPTIONAL MATCH (a)<-[like:LIKE]-(:User)
            MERGE (n)-[nru:RELATE_USER]->(u) 
            with
            toFloat(count( like)) as likes , n, rur
                set n.likeCount = likes
                detach delete rur
        `, { answerId: answerId, userId: userId }).then(res => {
        });
    }
    async createFollowNotification(userId, userTofollowId) {
        let responseObj;
        const user = await main_1.neode.first('User', { id: userId }, 1);
        const userToNotify = await main_1.neode.first('User', { id: userTofollowId }, 1);
        const pushNotificationTitle = user.get('username') != null ? user.get('username') : user.get('firstname') != null ? user.get('firstname') : 'Some user';
        if (user && userToNotify) {
            if (userTofollowId != userId) {
                const notificationType = notification_controller_1.NotificationType.FOLLOW;
                const notificationBody = userToNotify.get('privateProfile') ? 'Want to follow you' : 'Has started follow to you';
                const followStatus = userToNotify.get('privateProfile') ? notification_controller_1.FollowStatusNotification.PENDING : notification_controller_1.FollowStatusNotification.ACCEPTED;
                const response = await main_1.neode.writeCypher(`
                 Match (u: User {id:$userTofollowId})-[ut:USER_NOTIFICATIONS]->(nt: Notification {type: $notificationType})
                    -[ru:RELATE_USER]-> (ou:User {id:$userId}) return properties(nt) as notification
                `, { userId: userId, userTofollowId: userTofollowId, notificationType: notificationType });
                if (response.records.length < 1) {
                    const notification = await main_1.neode.merge('Notification', { type: notificationType, followStatus: followStatus });
                    await notification.relateTo(userToNotify, 'user');
                    await notification.relateTo(user, 'relatedUser');
                    if (userToNotify.get('notificationEnable') && userToNotify.get('followNotification')) {
                        this.sendFcmNotification([userToNotify.get('pushToken')], pushNotificationTitle, notificationBody, '');
                    }
                    responseObj = { code: 200, data: await notification.toJson() };
                }
                else {
                    const existNotification = response.records[0].get('notification');
                    if (existNotification.followStatus == notification_controller_1.FollowStatusNotification.DECLINED) {
                        main_1.neode.writeCypher(`Match (n: Notification {id:$id}) set n.followStatus = $followStatus `, { id: existNotification.id, followStatus: notification_controller_1.FollowStatusNotification.PENDING }).then((res) => { });
                    }
                    responseObj = { code: 200, data: 'Notification already exist' };
                }
            }
            else {
                responseObj = { code: 200, data: 'no notification required' };
            }
            return responseObj;
        }
        else {
            responseObj = { code: 422, data: 'The user or user to follow not found' };
            return responseObj;
        }
    }
    async createAskInterestQuestionNotification(userId, interestId, questionId) {
        let responseObj;
        await main_1.neode.writeCypher(`
            MATCH (i: Interest{ id:$interestId}) <- [:INTERESTED_IN] - (u: User ) , (ur:User{id:$userId}), (q:Question{id:$questionId})
            WHERE u <> ur
            MERGE (u)-[:USER_NOTIFICATIONS]-> (nt:Notification {type:"interestAsk", createdAt:$now, read: false})-[:RELATE_USER]-> (ur)
            MERGE (nt)-[:QUESTION_NOTIFY]-> (q)
            return u.id as id, u.pushToken as pushToken ,u.interestQuestionNotification as interestNotiticationActive , u.notificationEnable as notificationEnable
        `, { interestId: interestId, userId: userId, questionId: questionId, now: new Date().getTime() }).then((res) => {
            const tokens = [];
            res.records.forEach(x => {
                if (x.get('pushToken') != null && x.get('interestNotiticationActive') && x.get('notificationEnable')) {
                    tokens.push(x.get('pushToken'));
                }
            });
            if (tokens.length > 0) {
                this.sendFcmNotification(tokens, 'Stoppoint', 'New topic question for you', '');
            }
            responseObj = { code: 200, data: 'Success' };
        });
    }
    async createDirectQuestionNotification(questionId, userId) {
        const user = await main_1.neode.first('User', { id: userId }, 1);
        const question = await main_1.neode.first('Question', { id: questionId }, 1);
        const pushNotificationTitle = user.get('username') != null ? user.get('username') : user.get('firstname') != null ? user.get('firstname') : 'Some user';
        if (user && question) {
            const relationsip = question.get('userReceived');
            const userToNotify = relationsip.endNode();
            const notification = await main_1.neode.merge('Notification', { type: notification_controller_1.NotificationType.DIRECTQUESTION });
            await notification.relateTo(userToNotify, 'user');
            await notification.relateTo(question, 'question');
            await notification.relateTo(user, 'relatedUser');
            if (userToNotify.get('notificationEnable') && userToNotify.get('questionForYouNotification')) {
                this.sendFcmNotification([userToNotify.get('pushToken')], pushNotificationTitle, 'Sent a question for you ', '');
            }
        }
    }
    async existsNotificationTypeInQuestion(question, type) {
        const notifications = question.get('notifications');
        let exists = false;
        for (const x of notifications.map(x => { return x; })) {
            if ((await x.toJson()).node.type == type) {
                exists = true;
            }
        }
        return exists;
    }
    async existsNotificationTypeInAnswer(answer, type, relatedUser) {
        const notifications = answer.get('notifications');
        let exists = false;
        const verifyId = relatedUser.properties().id;
        for (const x of notifications.map(x => { return x; })) {
            const notification = await x.toJson();
            if (notification.node.type == type && notification.node.relatedUser.node.id == verifyId) {
                exists = true;
            }
        }
        return exists;
    }
    async existLikeNotificationOnAnswer(answerId) {
        return main_1.neode.readCypher(`
            MATCH (a:Answer {id:$answerId}) 
            OPTIONAL MATCH (a) <-[:ANSWER_NOTIFY]-(n:Notification {type:"like"}) return a, n
        `, { answerId: answerId }).then(res => {
            return res.records[0].get('n') != null;
        });
    }
    async sendFcmNotification(userPushTokens, title, body, imageUrl) {
        if (userPushTokens.length > 0 && userPushTokens[0].length > 0) {
            await this.fcmService.sendNotification(userPushTokens, {
                notification: {
                    title: title,
                    body: body,
                    image: imageUrl
                },
            }, true);
        }
    }
};
NotificationService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [nestjs_fcm_1.FcmService])
], NotificationService);
exports.NotificationService = NotificationService;
//# sourceMappingURL=notification.service.js.map