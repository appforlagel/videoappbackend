"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const constants_1 = require("../pgpromise/constants");
let ChatModel = class ChatModel {
    constructor(pg) {
        this.pg = pg;
    }
    dbGetLastByKind(username, maxConversations, maxMessages, kind) {
        return this.pg.any(`
            SELECT
                X.username,
                X.bare_peer,
                X.xml,
                X.txt,
                --TO_CHAR(X.created_at, 'DD/MM/YYYY') as created_at
                created_at
            FROM(
                SELECT 
                    archive.username,
                    archive.bare_peer,
                    archive.xml,
                    archive.txt,
                    archive.created_at,
                    archive.kind,
                    row_number() OVER(PARTITION BY archive.bare_peer ORDER BY archive.created_at DESC) as row_number
                FROM(
                    SELECT DISTINCT
                        bare_peer,
                        MAX(created_at) 
                    FROM archive
                    WHERE username = $1
                    GROUP BY bare_peer
                    LIMIT $2
                ) T
                INNER JOIN archive ON(T.bare_peer = archive.bare_peer)
            )AS X
            WHERE X.username = $1 AND X.row_number <= $3 AND X.kind = $4`, [username, maxConversations, maxMessages, kind]);
    }
    dbGetByKindFromTo(username, contact, limit, offset, kind) {
        return this.pg.any(`
            SELECT 
                username, 
                bare_peer,
                xml,
                txt, 
                created_at
            FROM archive
            WHERE username = $1 AND bare_peer = $2 AND kind = $5
            ORDER BY created_at DESC
            LIMIT $3 OFFSET $4`, [username, contact, limit, offset, kind]);
    }
};
ChatModel = __decorate([
    __param(0, common_1.Inject(constants_1.NEST_PGPROMISE_CONNECTION)),
    __metadata("design:paramtypes", [Object])
], ChatModel);
exports.ChatModel = ChatModel;
//# sourceMappingURL=chat.js.map