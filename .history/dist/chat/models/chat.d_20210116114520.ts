export declare class ChatModel {
    private readonly pg;
    constructor(pg: any);
    dbGetLastByKind(username: String, maxConversations: Number, maxMessages: Number, kind: String): any;
    dbGetByKindFromTo(username: String, contact: String, limit: Number, offset: Number, kind: String): any;
    dbDeleteByKindFromTo(username: String, contact: String, limit: Number, offset: Number, kind: String): any;
}
