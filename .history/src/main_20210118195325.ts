import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as Neode from 'neode';
import * as fs from 'fs';
const serverDb = 'bolt://3.129.18.242:7687';
const localDb = 'bolt://localhost:7687';
export const neode = new Neode(serverDb, 'neo4j', 'videoApp2020')
  .with({
    User: require('../src/auth/models/User'),
    Interest: require('../src/auth/models/Interest'),
    Question: require('../src/auth/models/Question'),
    Answer: require('../src/auth/models/Answer'),
    Notification: require('../src/auth/models/Notification'),
  });
async function bootstrap() {
  // const httpsOptions = {
  //   key: fs.readFileSync('./src/secrets/key.pem'),
  //   cert: fs.readFileSync('./src/secrets/cert.pem'),
  // };

  const app = await NestFactory.create(AppModule);
  app.setGlobalPrefix('api');
  app.enableCors();
  
  //neode.schema.install().then((res) => { console.log('schema installed') }).catch((err) => { console.log(err) });
  //neode.schema.drop();
  await app.listen(process.env.port || process.env.PORT || 3000);
}
bootstrap();
