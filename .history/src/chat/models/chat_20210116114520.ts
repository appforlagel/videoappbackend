import { Inject } from '@nestjs/common';
import { NEST_PGPROMISE_CONNECTION } from '../pgpromise/constants';

export class ChatModel {
    constructor(@Inject(NEST_PGPROMISE_CONNECTION) private readonly pg) { }

    public dbGetLastByKind(username: String, maxConversations: Number, maxMessages: Number, kind: String) {
        return this.pg.any(`
            SELECT
                X.username,
                X.bare_peer,
                X.xml,
                X.txt,
                --TO_CHAR(X.created_at, 'DD/MM/YYYY') as created_at
                created_at
            FROM(
                SELECT 
                    archive.username,
                    archive.bare_peer,
                    archive.xml,
                    archive.txt,
                    archive.created_at,
                    archive.kind,
                    row_number() OVER(PARTITION BY archive.bare_peer ORDER BY archive.created_at DESC) as row_number
                FROM(
                    SELECT DISTINCT
                        bare_peer,
                        MAX(created_at) 
                    FROM archive
                    WHERE username = $1
                    GROUP BY bare_peer
                    LIMIT $2
                ) T
                INNER JOIN archive ON(T.bare_peer = archive.bare_peer)
            )AS X
            WHERE X.username = $1 AND X.row_number <= $3 AND X.kind = $4`, [username, maxConversations, maxMessages, kind]);
    }

    public dbGetByKindFromTo(username: String, contact: String, limit: Number, offset: Number, kind: String) {
        return this.pg.any(`
            SELECT 
                username, 
                bare_peer,
                xml,
                txt, 
                created_at
            FROM archive
            WHERE username = $1 AND bare_peer = $2 AND kind = $5
            ORDER BY created_at DESC
            LIMIT $3 OFFSET $4`, [username, contact, limit, offset, kind]);
    }
    public dbDeleteByKindFromTo(username: String, contact: String, kind: String) {
        return this.pg.any(`
            DELETE 
            FROM archive
            WHERE username = $1 AND bare_peer = $2 AND kind = $5`,[username, contact,  kind]);
    }
}